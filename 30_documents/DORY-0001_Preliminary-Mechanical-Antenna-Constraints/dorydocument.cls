\ProvidesClass{dorydocument}%2022/23 DORY working document Template LaTeX class

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%% General Settings  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\mytitle}{Preliminary Mechanical Antenna Constraints}
\newcommand{\mytype}{Requirements Specification}
\newcommand{\myauthors}{Martin Stempfle, Lena Hennige, Paul Droste}
\newcommand{\issue}{1.0}
\newcommand{\mynumber}{DORY-0001}

\author{\myname}
\title{\mytitle}
\date{\mydateformat\today}

\LoadClass[ 
    paper       = a4,                       % DIN-A4 (Standard)			            
    fontsize	= 12pt,	                    % Schriftgröße
    twoside		= false,		            % einseitiger Druck (Standard)
    draft		= false,                    % Hinweisboxen nicht anzeigen
    DIV 		= calc,				        % Satzspiegel selbst berechnen	
    bibliography= totoc,                    % Quellenverzeichnis ins Inhaltsverzeichnis
    listof		= totoc,                    % Verzeichnisse ins Inhaltsverzeichnis
    listof		= chaptergapline,
    listof      = notnumbered,              % Verzeichnisse ohne Nummerierung
    listof      = entryprefix,
    headheight  = 20mm,
    footheight  = 10mm
	]{scrartcl}                             % scrreprt: Dokumentklasse des KOMA-Skriptes für längere Projektarbeiten
	        
\usepackage{scrhack}                        % KOMA-Skript-Erweiterung


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%% Packages and Settings %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[T1]{fontenc}                    % westeuropäische ASCII-Kodierung 
\usepackage[utf8]{inputenc}                 % Kodierung zur Erkennung von Sonderzeichen im Text
\usepackage[english]{babel} 	            % englische Rechtschreibung und Silbentrennung
\usepackage{lmodern}				        % moderne Latex-Schriftart

\usepackage{blindtext}

\usepackage{graphicx} 				        % Grafikeinfügeoption
\graphicspath{graph} 				        % Standard Grafikpfad
\usepackage{color, xcolor}                  % Farben
\usepackage{epstopdf}                       % Zum Importieren von eps Dateien
\usepackage[
    justification=centering,
    font=sf, 
    labelfont=bf
	]{caption}						        % Formatierung von Beschriftungen
\usepackage{subcaption}                     % subfigures erstellen

\usepackage{tabularx}				        % Tabellenoption für Tabellen mit festgelegter Breite ggf. und mehrzeiligen Zellen
\usepackage{tabu}					        % bestimmtes Längenverhältnis für X-Spalten aus tabularx festlegen
\usepackage{array}					        % neue Spaltentypen für Tabellen definieren
\usepackage{multirow}				        % Zellen in Zeile zusammenführen
\usepackage{booktabs,longtable}             % Zusätzliche Optionen für Tabellen

\usepackage{pdflscape}				        % PDF-Seiten im Querformat
\usepackage{pdfpages}				        % PDF-Seiten einfügen
\usepackage{rotating}				        % Objekte rotieren

\usepackage[hyphens]{url}			        % Zeilenumbruch in Urls nach Bindestrich erzwingen

\usepackage{siunitx}				        % SI-Einheiten
\usepackage{amsmath, amssymb, amsthm, amsfonts} % Math. Gleichungen und Symbole

\usepackage{datetime}                       % Heutiges Datum mit unterschiedlichen Formaten einfügen
\def\twodigits#1{\ifnum #1<10 0\fi\number #1}
\newdateformat{mymonthformat}{\monthname[\THEMONTH] \THEYEAR}
\newdateformat{mydateformat}{\THEYEAR-\twodigits\THEMONTH-\THEDAY}

\usepackage[
    paper			= a4paper,		        % Din-A4-Seite
	hmargin			= 15mm, 		        % Randabstand rechts und links
	top				= 10mm,			        % Abstand zwischen Kopfzeile und oberem Seitenrand
	head			= 20mm, 		        % Höhe der Kopfzeile
	headsep			= 15mm, 		        % Abstand zwischen Kopfzeile und Text
	foot			= 17.5mm, 		        % Höhe der Fußzeile
	bottom			= 25mm,			        % Abstand zwischen unterem Rand und Fußzeile
	heightrounded	= true, 		        % rundet "\textwidth" (Länge des Textfeldes) auf ein ganzzahliges Vielfaches von "\baselineskip" plus "\topskip"
	includeheadfoot,					    % setzt "includehead" und "includefoot" auf "true"
    footskip = \dimexpr\headsep+\ht\strutbox\relax,
    tmargin = 0mm,
    bmargin = \dimexpr17mm+2\ht\strutbox\relax
	]{geometry}						        % geometry-Paket zum individuellen Festlegen des Seitenlayout
 
\usepackage{setspace}                       % Einfügen von Abständen

\usepackage[
	headsepline, 							% horizontale Trennlinie unter Kopfzeile 
	]{scrlayer-scrpage}						% scrlayer-scrpage-Paket zum Modifizieren des Kopf- und Fußzeilenlayouts					

% Konfiguration der Kopf- und Fußzeile
\clearpairofpagestyles
\clearscrheadfoot
\ihead{ \begin{minipage}[t]{0.2\linewidth}
            \includegraphics[height=1.5cm]{graph/seesat_logo_dark2.png}
        \end{minipage}}
\chead{ \begin{minipage}[t]{0.5\linewidth}
            \centering\textbf{\mynumber} \\
            \centering\mytitle
        \end{minipage}}
\ohead{ \begin{minipage}[t]{0.2\linewidth}
            \begin{tabular}{l l}
                Date:   &   \mydateformat\today \\
                Issue:  &   \issue \\
                Page:   &   \pagemark
            \end{tabular}
        \end{minipage}}
\automark{section}
\setheadsepline{.5pt}

% Konfiguration des Titels
\makeatletter
\def\@maketitle{
    \null
	\centering\includegraphics[height=4cm]{graph/seesat_logo_dark2.png}
  
    \vskip 5em%
    \begin{center}
        \begin{Large}
            \textbf{\@title}\\
        \end{Large}
        \vskip 2em
        
        \begin{large}
            \mynumber
        \end{large}

        \vskip 2em
        \begin{large}
            \mytype
        \end{large}
    \end{center}

    \vfill

    \begin{center}
        \begin{large}
            \begin{tabular}{l l}
            Prepared by: & \myauthors \\
            Checked by: & \\
            \end{tabular}
        \end{large}
    \end{center}

    \vfill

    \begin{center}
        \begin{tabular}{l l}
            Date: & \mydateformat\today \\
            Issue: & \issue \\   
        \end{tabular}
    \end{center}
}

% Konfiguration der Kurzfassung
\renewcommand{\abstractname}{Abstract}

\usepackage[						
	backend			= biber,		        % biber-Software erstellt Literaturverzeichnis
	bibencoding		= utf8,			        % Sonderzeichen in Literaturangaben zulassen
	sorting			= none,			        % Sortierung der Quellen und Zitate nach Reihenfolge im Text
	hyperref		= true,			        % Hyperlinks führen durch Klick auf Quellenverweis im Text direkt zu Literaturverzeichnis
	giveninits		= false,		        % Vornamen der Autoren abkürzen
	isbn        	= false,		        % ISBN nicht anzeigen
	abbreviate		= false,		        % Abkürzungen nicht anzeigen
	url				= true,			        % URL anzeigen
%	howpublished	= true,
%	publisher		= true,			        % Verlag anzeigen		
    style           = numeric               % Festlegen des Literaturverzeichnis Layout
	]{biblatex}
%\bibliographystyle{numeric}                 % Festlegen des Literaturverzeichnis Layout
\addbibresource{texfiles/literature.bib}	% Datenbank mit Auflistung der Literaturquellen hinzufügen 

% URLs nicht über Zeilenrand hinaus abdrucken
\setcounter{biburlnumpenalty}{50}
\setcounter{biburlucpenalty}{50}
\setcounter{biburllcpenalty}{50}

\usepackage[
	colorlinks		= false,		         % keine farbigen Hyperlinks
	allbordercolors	= {white},		         % Ränder um Hyperlinks sind weiß (kommt farbloser Einstellung am nächsten)
	unicode			= true						
	]{hyperref}
\usepackage[all]{hypcap}			         % Hyperlinks liegen auf Bild anstelle von Bildunterschrift

% PDF-Informationen einbauen
\hypersetup{
    hidelinks,
    pdftex,
	pdftitle   	= {\mytitle}, 
	pdfsubject 	= {Abstract},
	pdfkeywords = {Antenna, VHF, Automatic Identification System}
}

\usepackage[		
	acronyms,					% Abkürzungsverzeichnis separat zu Symbolverzeichnis
	nopostdot,                	% kein Punkt nach der Beschreibung im Verzeichnis
	toc,						% ins Inhaltsverzeichnis aufnehmen
	section,					% als "sections"
	nomain,                   	% kein normales Glossar
	nonumberlist				% keine Seitenzahlen
	]{glossaries}               % Erstellen von Abkürzungs- oder Formelverzeichnis
 \usepackage{glossary-mcols}    % Mehrere Spalten für Formelverzeichnis
 \usepackage{glossaries-extra}
 \usepackage{glossary-longbooktabs}

\usepackage{verbatim}           % Einfachen Quellcode formatieren
\usepackage{listings}			% Quellcode formatieren

% Farben für Quellcode definieren
\definecolor{gruen_code}{rgb}{0,0.6,0}
\definecolor{grau_code}{rgb}{0.5,0.5,0.5}
\definecolor{lila_code}{rgb}{0.58,0,0.82}
\definecolor{grau_hell_code}{rgb}{0.97,0.97,0.94}

\lstdefinestyle{mystyle}{
    backgroundcolor		= \color{grau_hell_code},  	% Hintergrund hellgrau
    commentstyle		= \color{gruen_code},		% Kommentare grün
    keywordstyle		= \color{blue},				% Schlüsselworte magenta
    numberstyle			= \tiny\color{grau_code},	% Zeilennummerierung grau
    stringstyle			= \color{lila_code},		% Strings lila
    basicstyle			= \ttfamily\footnotesize,	% Schriftbild und -größe
    breakatwhitespace	= false,         									
    breaklines			= true,                 	% zu lange Codezeilen umbrechen
    captionpos			= b,                    								
    keepspaces			= true,                 	% Leerzeichen beibehalten
    numbers				= left,    					% Zeilennummerierung links                
    numbersep			= 5pt,                 		% Zeilennummerierung 5pt von Quellcode entfernt
    showspaces			= false,               		% Leerzeichen nicht kennzeichnen
    showstringspaces	= false,					% Leerzeichen in Strings nicht kennzeichnen
    showtabs			= false,               		% Tabs nicht kennzeichnen
    tabsize				= 2							% Anzahl Leerzeichen pro Tab
}

\usepackage{textcomp}                               % Zusätzliche Symbole ung Zeichen
\usepackage{microtype}                              % Verbesserte Textformatierung

\usepackage[titletoc,title,header]{appendix}        % Hinzufügen des Anhangs

\newcommand{\imagewidth}{0.85\textwidth}            % Definition der Standard Bildbreite

\renewcommand{\autodot}{}                           % Remove all end-of-counter dots
\widowpenalty	= 10000				                % keine einzelnen Zeilen eines Absatzes am oberen Ende einer Seite
\clubpenalty	= 10000				                % keine einzelnen Zeilen eines Absatzes am unteren Ende einer Seite

\newcounter{prevcnt}                                % Zähler für unterschiedliche Seitenzahl (Roman/Arabic)

% Konfiguration von Bezeichnungen
\addto\captionsenglish{\def\figurename{Fig.}}
\addto\extrasenglish{\def\figureautorefname{Fig.}}
\addto\captionsenglish{\def\tablename{Tab.}}
\addto\extrasenglish{\def\tableautorefname{Tab.}}

\renewcaptionname{english}\sectionautorefname{Chapter}
\renewcaptionname{english}\subsectionautorefname{Section}
\renewcaptionname{english}\subsubsectionautorefname{Subsection}

\renewcommand*\sectionmarkformat{}

\makeglossaries                                     % Erstellung der Verzeichnisse
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%