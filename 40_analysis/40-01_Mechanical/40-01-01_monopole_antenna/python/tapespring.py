"""

Contains useful functions for calculations involving tape springs

"""

import numpy as np
from scipy import optimize
from linearSpring import LinearSpring
import laminate


class TapespringReel:

    def __init__(self, radius):
        self.radius = radius


class Tapespring:

    def __init__(self, transverse_curvature, sector_angle, length, thickness,
                 laminate: laminate.OrthotropicLaminate):
        self.__transverse_curvature = transverse_curvature
        self.__sector_angle = sector_angle
        self.length = length
        self.thickness = thickness
        self.laminate = laminate

    @property
    def transverse_curvature(self):
        return self.__transverse_curvature

    @transverse_curvature.setter
    def transverse_curvature(self, new_transverse_curvature):
        if new_transverse_curvature <= 0:
            raise ValueError("Tapespring curvature must be positive!")

        else:
            self.__transverse_curvature = new_transverse_curvature

    @property
    def sector_angle(self):
        return self.__sector_angle

    @sector_angle.setter
    def sector_angle(self, new_sector_angle):
        if new_sector_angle <= 0:
            raise ValueError("Tapespring curvature must be positive!")

        else:
            self.__transverse_curvature = new_sector_angle

    def arc_length(self):
        return self.sector_angle / self.transverse_curvature


class TapespringAssembly():

    def __init__(self, tapespring: Tapespring,
                 tapespring_number,
                 reel: TapespringReel,
                 spring: LinearSpring,
                 spring_number,
                 buffer_length,
                 layer_friction_coefficient):
        self.tapespring = tapespring
        self.tapespring_number = tapespring_number
        self.reel = reel
        self.spring = spring
        self.spring_number = spring_number
        self.buffer_length = buffer_length
        self.layer_friction_coefficient = layer_friction_coefficient

        self.energy_density_distribution_in_coiled_boom_nominal = \
            self.__energy_density_distribution_in_coiled_boom()

    def pitch(self):
        return self.tapespring.thickness / (2 * np.pi) * self.tapespring_number

    def number_of_layers(self, coiled_angle):
        return self.tapespring_number * coiled_angle / 2 / np.pi

    def end_radius_nominal(self, angle):
        return self.end_radius(self.reel.radius, angle)

    def end_radius(self, inner_radius, angle):
        return inner_radius + angle * self.pitch()

    def coiled_length(self, angle):
        return self.__tapespring_length_at_angle(angle) - \
            self.__tapespring_length_at_angle(self.start_angle())

    def coiled_length_blooming(self, coiled_angle, blooming_inner_radius):
        blooming_start_angle = blooming_inner_radius/self.pitch()

        return self.__tapespring_length_at_angle(coiled_angle +
                                                 blooming_start_angle) - \
            self.__tapespring_length_at_angle(blooming_start_angle)

    def start_angle(self):
        return self.reel.radius / self.pitch()

    def nominal_end_radius(self):
        return self.__nominal_end_angle() * self.pitch()

    def __nominal_end_angle(self):
        """
        Returns angle at which tapespring ends with angle 0 being set as the
        start of a hypothetical tapespring spiral beginning at the reel axis.
        :return:
        """
        result = optimize.root_scalar(self.__find_root_tapespring_end_angle,
                                      bracket=[0.9 * self.start_angle(),
                                               1000 * np.pi])

        if not result.converged:
            raise UserWarning("Root search for end of tape spring did not "
                              "converge.")

        return result.root

    def __find_root_tapespring_end_angle(self, x):
        """
        Root equation to find spring end angle if tapespring length is fully
        stored on reel. Angle is calculated with 0 being set as the start of a
        hypothetical tapespring spiral beginning at the reel axis.
        :param x: angle variable to find root for
        :return:  deviation of length calculated at angle 'x' and tapespring
        length incl. buffer length
        """
        return self.__tapespring_length_at_angle(x) - \
            self.__tapespring_length_at_angle(self.start_angle()) - \
            (self.tapespring.length + self.buffer_length)

    def __tapespring_length_at_angle(self, angle):
        return self.pitch() / 2 * (angle * np.sqrt(1 + angle ** 2) +
                                   np.arcsinh(angle))

    def total_deployment_torque(self):
        """
        Returns the total deployment torque over the course of boom
        deployment. The given angle is equivalent to the angle traversed by
        the hub
        :return:
        """
        # calculate torque from normal deployment
        traversed_hub_angle, nominal_boom_torque = \
            self.boom_deployment_torque_by_deployment_angle_nominal()

        nominal_boom_torque *= self.tapespring_number

        coiled_angle = np.flip(traversed_hub_angle)

        # calculate blooming torque
        blooming_torque, blooming_inner_radius, blooming_end_radius = \
            self.boom_deployment_torque_by_deployment_angle_blooming()

        blooming_torque *= self.tapespring_number

        # calculate torque from spring energy being stored
        spring_energy = self.__spring_energy(blooming_end_radius)
        spring_energy_torque = np.gradient(spring_energy, traversed_hub_angle)

        # calculate friction torque
        # the contacting length is divided by the number of tapesprings,
        # since multiple layers decrease the speed at which the friction occurs

        contacting_length = self.__contacting_length_blooming(
            coiled_angle, blooming_inner_radius)

        contacting_length_derivative = np.gradient(contacting_length,
                                                   traversed_hub_angle)

        friction_torque = self.__friction_torque(blooming_end_radius,
                                                 contacting_length_derivative,
                                                 coiled_angle)

        return traversed_hub_angle, blooming_inner_radius, \
            nominal_boom_torque,\
            blooming_torque,\
            spring_energy_torque,\
            friction_torque

    def boom_deployment_torque_by_deployment_angle_nominal(self):
        # get coiled length and energy density at coiled length
        energy_density_integration_points = \
            self.energy_density_distribution_in_coiled_boom_nominal

        traversed_hub_angle_integration_points = self.__traversed_hub_angle()

        # convert coiled length to end radius
        radius_integration_points = \
            self.end_radius_nominal(traversed_hub_angle_integration_points)

        # calculate nominal deployment torque
        torque = energy_density_integration_points * \
                 radius_integration_points * \
                 self.tapespring.arc_length()

        return traversed_hub_angle_integration_points, torque

    def boom_deployment_torque_by_deployment_angle_blooming(self):
        # get traversed hub angle and conversely coiled angle integration points
        traversed_hub_angle_integration_points = self.__traversed_hub_angle()

        coiled_angle = np.flip(traversed_hub_angle_integration_points)

        # find inner radius in blooming for each traversed hub angle point
        inner_radius = []
        for ca in coiled_angle:
            inner_radius.append(self.__find_inner_blooming_radius(ca))

        energy = []
        end_radius = []

        for i in range(0, len(inner_radius)):
            # select applicable hub angles
            applicable_hub_angles = np.flip(coiled_angle[i:])

            # construct radius integration points
            radius_integration_points = \
                self.end_radius(inner_radius[i],
                                applicable_hub_angles)

            # append end_radius to return array
            end_radius.append(radius_integration_points[-1])

            energy_density_integration_points = \
                self.__energy_density_distribution_in_coiled_boom(
                    radius_integration_points)

            # calculate blooming torque
            energy_per_angle = energy_density_integration_points * \
                               radius_integration_points * \
                               self.tapespring.arc_length()

            energy.append(np.trapz(energy_per_angle,
                                   applicable_hub_angles))

        torque = np.gradient(energy, traversed_hub_angle_integration_points)

        return torque, np.array(inner_radius), np.array(end_radius)

    def __find_inner_blooming_radius(self, coiled_angle):
        # find root of inner blooming radius
        result = optimize.root_scalar(self.__find_root_inner_blooming_radius,
                                      coiled_angle,
                                      x0=self.reel.radius,
                                      bracket=[0.8 * self.reel.radius,
                                               100 * self.reel.radius])

        if not result.converged:
            raise UserWarning("Root search for inner radius of blooming tape "
                              "spring did not converge.")

        return result.root

    def __find_root_inner_blooming_radius(self, x, coiled_angle):
        angle_of_x = x / self.pitch()

        # root equation for inner radius x
        return self.__tapespring_length_at_angle(
            coiled_angle + angle_of_x) - \
            self.__tapespring_length_at_angle(angle_of_x) - \
            (self.tapespring.length + self.buffer_length)

    def __traversed_hub_angle(self):
        hub_initial_traversed_angle = \
            self.__nominal_end_angle() - self.start_angle()

        # this doesn't start at zero due to some numerical instabilities in
        # the calculations for very low angles
        traversed_hub_angle_integration_points = \
            np.arange(start=np.pi / 3,
                      stop=hub_initial_traversed_angle,
                      step=np.pi / 36)

        return traversed_hub_angle_integration_points

    def __energy_density_distribution_in_coiled_boom(self,
                                                     radius_integration_points=None):
        if radius_integration_points is None:
            traversed_hub_angle_integration_points = self.__traversed_hub_angle()

            radius_integration_points = \
                self.end_radius_nominal(traversed_hub_angle_integration_points)

        curvature_x_integration_points = 1 / radius_integration_points

        energy_density_integration_points = []

        for k_x in curvature_x_integration_points:
            energy_density_integration_points.append(
                self.__energy_state_from_global_curvature(k_x))

        energy_density_integration_points = \
            np.array(energy_density_integration_points)

        return energy_density_integration_points

    def __energy_state_from_global_curvature(self, curvature_x):
        global_deformation = laminate.DeformationState(
            np.array([[0], [0], [0]]),
            np.array([[curvature_x],
                      [self.tapespring.transverse_curvature],
                      [0]]))

        self.tapespring.laminate.deformation = global_deformation

        return self.tapespring.laminate.energy_density()

    def __spring_energy(self, end_radius):
        deformation = end_radius - end_radius[0]
        return - self.spring_number * \
            (self.spring.force(0) * deformation +
             0.5 * self.spring.stiffness * deformation ** 2)

    def spring_force(self, end_radius):
        deformation = end_radius - end_radius[0]
        return (self.spring.force(0) +
                self.spring.stiffness * deformation)

    def __friction_torque(self, end_radius,
                          contacting_length_derivative_on_angle,
                          coiled_angle):
        return self.__total_pressure_between_layers(end_radius, coiled_angle) * \
            self.layer_friction_coefficient * \
            contacting_length_derivative_on_angle

    def __total_pressure_between_layers(self, end_radius, coiled_angle):
        """
        Semi-empirical formula for total pressure between layers as described
        by Wang et al., 2024
        :param end_radius: radius at end of coiled boom region
        :return:
        """
        number_layers_under_friction = (self.number_of_layers(coiled_angle) - 1)
        number_layers_under_friction[number_layers_under_friction < 0] = 0

        return 2 * np.power(number_layers_under_friction, 0.7) * \
            (self.spring_force(end_radius) +
             1.2 * self.tapespring.arc_length() *
             self.tapespring.laminate.D()[0, 0] *
             self.tapespring.transverse_curvature ** 2)

    def __contacting_length_blooming(self, coiled_angle, blooming_inner_radius):
        contacting_length = \
            self.coiled_length_blooming(
                coiled_angle - 2 * np.pi, blooming_inner_radius)

        contacting_length *= self.tapespring_number

        # replace negative values with zero, as friction between reel and
        # tapespring is ignored and negative lengths do not make sense
        contacting_length[contacting_length < 0] = 0

        return contacting_length
