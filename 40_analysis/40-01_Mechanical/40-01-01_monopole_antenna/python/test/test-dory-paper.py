"""
Calculates the deployment dynamics for several materials. Used for the
creation of the DLRK 2024 DORY paper.
"""
import pandas as pd

import laminate
from tapespring import Tapespring, TapespringReel, TapespringAssembly
from linearSpring import LinearSpring

import numpy as np
import pandas as pd

import json
import sys

if sys.argv[1] == "materials":
    # config
    export_dir = "test-dory-paper-export"
    config_sign = "config1"

    # import config
    config_file = open("config/config.json")
    config_dict = json.load(config_file)

    config_dict = config_dict[config_sign]

    # geometrical parameters
    tapespring_thickness = config_dict["tapespring_thickness"]
    tapespring_sector_angle = config_dict["tapespring_sector_angle"]/180 * np.pi
    tapespring_length = config_dict["tapespring_length"]
    k_y = config_dict["k_y"]

    radius_reel = config_dict["radius_reel"]

    spring_stiffness = config_dict["spring_stiffness"]
    spring_initial_force = config_dict["spring_initial_force"]

    tapespring_number = config_dict["tapespring_number"]
    spring_number = config_dict["spring_number"]
    buffer_length = config_dict["buffer_length"]
    layer_friction_coefficient = config_dict["layer_friction_coefficient"]

    reel = TapespringReel(radius_reel)
    spring = LinearSpring(spring_stiffness, spring_initial_force/spring_stiffness)

    # import material data
    material_file = open("../data/materials_dory_dlrk2024.json")
    material_dict = json.load(material_file)

    for material, parameters in material_dict.items():

        E1 = parameters["E1"]
        E2 = parameters["E2"]
        G12 = parameters["G12"]
        v12 = parameters["nu12"]

        initial_deformation = laminate.DeformationState(np.array([[0], [0], [0]]),
                                                        np.array([[0], [0], [0]]))

        neutral_deformation = laminate.DeformationState(np.array([[0], [0], [0]]),
                                                        np.array([[0], [k_y], [0]]))


        tapespring_lamina = laminate.OrthotropicLamina(
            E1=E1, E2=E2, G12=G12, v12=v12, v21=v12*E2/E1,
            initial_deformation=initial_deformation, theta=0)

        tapespring_laminate = \
            laminate.OrthotropicLaminate(
                [tapespring_lamina],
                [-tapespring_thickness / 2, tapespring_thickness / 2],
                deformation=neutral_deformation)

        tapespring = Tapespring(k_y, tapespring_sector_angle, tapespring_length,
                        tapespring_thickness,
                        laminate=tapespring_laminate)

        tapespring_assembly = TapespringAssembly(
            tapespring=tapespring,
            tapespring_number=tapespring_number,
            reel=reel, spring=spring,
            spring_number=spring_number,
            buffer_length=buffer_length,
            layer_friction_coefficient=layer_friction_coefficient)

        traversed_hub_angle, blooming_inner_radius, \
            nominal_torque,\
            blooming_torque,\
            spring_energy_torque,\
            friction_torque = \
            tapespring_assembly.total_deployment_torque()

        export = pd.DataFrame(data={"traversed_hub_angle_rad":
                                        traversed_hub_angle,
                                    "blooming_inner_radius_m":
                                        blooming_inner_radius,
                                    "nominal_torque_Nm":
                                        nominal_torque,
                                    "blooming_torque_Nm":
                                        blooming_torque,
                                    "spring_energy_torque_Nm":
                                        spring_energy_torque,
                                    "friction_torque_Nm":
                                        friction_torque,
                                    "tapespring_thickness":
                                        tapespring_thickness,
                                    "tapespring_sector_angle_rad":
                                        tapespring_sector_angle,
                                    "tapespring_length_m":
                                        tapespring_length,
                                    "k_y_m_-1":
                                        k_y,
                                    "radius_reel_m":
                                        radius_reel,
                                    "spring_stiffness_N_m":
                                        spring_stiffness,
                                    "spring_initial_force_N":
                                        spring_initial_force,
                                    "tapespring_number":
                                        tapespring_number,
                                    "spring_number":
                                        spring_number,
                                    "buffer_length_m":
                                        buffer_length,
                                    "layer_friction_coefficient":
                                        layer_friction_coefficient
                                    }
                              )

        export_path = f"{export_dir}/{material}_{config_sign}.csv"

        export.to_csv(export_path, index=False)

if sys.argv[1] == "geometry":
    # config
    export_dir = "geometry-dory-paper-export"
    config_sign = "config1"
    material = "T700S-Twill"

    sector = np.array([105, 135, 165])
    radius = np.array([10, 12, 14])

    # import config
    config_file = open("config/config.json")
    config_dict = json.load(config_file)

    config_dict = config_dict[config_sign]

    # geometrical parameters
    tapespring_thickness = config_dict["tapespring_thickness"]
    tapespring_length = config_dict["tapespring_length"]

    radius_reel = config_dict["radius_reel"]

    spring_stiffness = config_dict["spring_stiffness"]
    spring_initial_force = config_dict["spring_initial_force"]

    tapespring_number = config_dict["tapespring_number"]
    spring_number = config_dict["spring_number"]
    buffer_length = config_dict["buffer_length"]
    layer_friction_coefficient = config_dict["layer_friction_coefficient"]

    reel = TapespringReel(radius_reel)
    spring = LinearSpring(spring_stiffness,
                          spring_initial_force / spring_stiffness)

    # import material data
    material_file = open("../data/materials_dory_dlrk2024.json")
    material_dict = json.load(material_file)

    # set material parameters
    parameters = material_dict[material]
    E1 = parameters["E1"]
    E2 = parameters["E2"]
    G12 = parameters["G12"]
    v12 = parameters["nu12"]

    for phi in sector:

        for r in radius:

            k_y = 1/(0.001*r)
            tapespring_sector_angle = phi/180*np.pi

            initial_deformation = laminate.DeformationState(
                np.array([[0], [0], [0]]),
                np.array([[0], [0], [0]]))

            neutral_deformation = laminate.DeformationState(
                np.array([[0], [0], [0]]),
                np.array([[0], [k_y], [0]]))

            tapespring_lamina = laminate.OrthotropicLamina(
                E1=E1, E2=E2, G12=G12, v12=v12, v21=v12 * E2 / E1,
                initial_deformation=initial_deformation, theta=0)

            tapespring_laminate = \
                laminate.OrthotropicLaminate(
                    [tapespring_lamina],
                    [-tapespring_thickness / 2, tapespring_thickness / 2],
                    deformation=neutral_deformation)

            tapespring = Tapespring(k_y, tapespring_sector_angle, tapespring_length,
                                    tapespring_thickness,
                                    laminate=tapespring_laminate)

            tapespring_assembly = TapespringAssembly(
                tapespring=tapespring,
                tapespring_number=tapespring_number,
                reel=reel, spring=spring,
                spring_number=spring_number,
                buffer_length=buffer_length,
                layer_friction_coefficient=layer_friction_coefficient)

            traversed_hub_angle, blooming_inner_radius, \
                nominal_torque, \
                blooming_torque, \
                spring_energy_torque, \
                friction_torque = \
                tapespring_assembly.total_deployment_torque()

            export = pd.DataFrame(data={"traversed_hub_angle_rad":
                                        traversed_hub_angle,
                                        "blooming_inner_radius_m":
                                            blooming_inner_radius,
                                        "nominal_torque_Nm":
                                            nominal_torque,
                                        "blooming_torque_Nm":
                                            blooming_torque,
                                        "spring_energy_torque_Nm":
                                            spring_energy_torque,
                                        "friction_torque_Nm":
                                            friction_torque,
                                        "tapespring_thickness":
                                            tapespring_thickness,
                                        "tapespring_sector_angle_rad":
                                            tapespring_sector_angle,
                                        "tapespring_length_m":
                                            tapespring_length,
                                        "k_y_m_-1":
                                            k_y,
                                        "radius_reel_m":
                                            radius_reel,
                                        "spring_stiffness_N_m":
                                            spring_stiffness,
                                        "spring_initial_force_N":
                                            spring_initial_force,
                                        "tapespring_number":
                                            tapespring_number,
                                        "spring_number":
                                            spring_number,
                                        "buffer_length_m":
                                            buffer_length,
                                        "layer_friction_coefficient":
                                            layer_friction_coefficient
                                        }
                                  )

            export_path = \
                f"{export_dir}/{material}_{config_sign}_r{r}_phi{phi}.csv"

            export.to_csv(export_path, index=False)
