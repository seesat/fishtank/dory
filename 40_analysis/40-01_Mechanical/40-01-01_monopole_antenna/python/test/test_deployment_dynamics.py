"""
Tests the tapespring deployment dynamics utilities by calculating the results
for a reference boom investigated in published studies
(Hoskin, 2018: "Blossoming of coiled deployable booms",
 Wang et al., 2020: "Tip force and pressure distribution analysis of a
 deployable boom during blossoming")
"""

import laminate
from tapespring import Tapespring, TapespringReel, TapespringAssembly
from linearSpring import LinearSpring

import numpy as np
import matplotlib.pyplot as plt

E = 205e+9
v12 = 0.3
t = 0.13e-3
G = 9e+9

k_y = 1/0.0125

initial_deformation = laminate.DeformationState(np.array([[0], [0], [0]]),
                                                np.array([[0], [0], [0]]))

neutral_deformation = laminate.DeformationState(np.array([[0], [0], [0]]),
                                                np.array([[0], [k_y], [0]]))

l1 = laminate.OrthotropicLamina(E1=E, E2=E, G12=G, v12=v12, v21=v12,
                                initial_deformation=initial_deformation,
                                theta=0)

test_laminate = \
    laminate.OrthotropicLaminate([l1], [-t / 2, t / 2],
                                 deformation=neutral_deformation)

ts = Tapespring(k_y, 0.81 * np.pi, 0.6, t,
                laminate=test_laminate)

r = TapespringReel(0.015, 0)
s = LinearSpring(10, 0.6)

tsa = TapespringAssembly(ts, 1, r, s, 4, 0.0, 0.25)

angle, torque = tsa.total_deployment_torque()

radius_at_tip = tsa.end_radius_nominal(np.flip(angle))

force = torque / radius_at_tip

# plot data

fig, ax = plt.subplots(2, 1)
ax[0].plot(angle[angle < angle.max()-2*np.pi]/(2*np.pi),
           torque[angle < angle.max()-2*np.pi])
ax[0].set_xlabel("number of turns")
ax[0].set_ylabel("torque applied to hub [Nm]")

ax[1].plot(angle[angle < angle.max()-2*np.pi]/(2*np.pi),
           force[angle < angle.max()-2*np.pi])
ax[1].set_xlabel("number of turns")
ax[1].set_ylabel("force applied by boom [N]")
plt.show()
