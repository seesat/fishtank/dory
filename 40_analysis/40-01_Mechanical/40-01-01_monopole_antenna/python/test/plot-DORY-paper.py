"""
Plots the boom deployment dynamics data generated by the python utilities.
"""

import sys
import os

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


seesat_colours = {"c1": "#800000",
                  "c2": "#bd942a",
                  "c3": "#31ACB4",
                  "c4": "#606060",
                  "c5": "#35d472",
                  "c6": "#e1001a"
                  }

if sys.argv[1] == "plot_boom_deployment":

    # config
    directory = "test-dory-paper-export"
    plot_dir = "plot-dory-paper"
    plot_name = "torque"

    fig_width = 8
    fig_height = 3.5

    ylim_torque = [-0.4, 0.6]
    ylim_force = [-1, 5]

    try:
        # select data
        filenames = [file.name for file in os.scandir(directory)]

        selected_filenames = [filename for filename in filenames
                              if f"_{sys.argv[2]}.csv" in filename]

        selected_filenames = sorted(selected_filenames)

        # import data
        data = {}
        for file in selected_filenames:
            data[file.removesuffix(f"_{sys.argv[2]}.csv")] = \
                pd.read_csv(f"{directory}/{file}")

        # plot data
        fig, ax = plt.subplots(2,1,
                               sharex=True)

        for index in range(0, len(data)):
            # form data
            material = list(data.keys())[index]
            results = list(data.values())[index]
            colour = list(seesat_colours.values())[index%len(
                seesat_colours.values())]

            # convert data
            hub_angle = np.array(results["traversed_hub_angle_rad"])
            nominal_torque = np.array(results["nominal_torque_Nm"])
            blooming_torque = np.array(results["blooming_torque_Nm"])
            spring_energy_torque = np.array(results["spring_energy_torque_Nm"])
            friction_torque = np.array(results["friction_torque_Nm"])
            tip_radius = np.array(results["blooming_inner_radius_m"])
            torque = nominal_torque + blooming_torque + spring_energy_torque

            tapespring_number = results["tapespring_number"][0]

            # remove last half coils due to high instability without friction
            # effect
            total_coiled_angle = hub_angle.max()
            torque = torque[hub_angle < hub_angle.max() - np.pi]
            tip_radius = tip_radius[hub_angle < hub_angle.max() - np.pi]

            hub_angle = hub_angle[hub_angle < hub_angle.max() - np.pi]
            coiled_angle = total_coiled_angle - hub_angle

            # plot torque
            ax[0].plot(coiled_angle/(2*np.pi),
                       torque,
                       color=colour,
                       label=material,
                       linewidth=1)

            ax[0].set_ylabel("torque on reel [Nm]")

            ax[0].set_ylim(ylim_torque)

            # plot tip force
            ax[1].plot(coiled_angle/(2*np.pi),
                       torque/tip_radius/tapespring_number,
                       color=colour,
                       label=material,
                       linewidth=1)

            ax[1].set_ylabel("tip force [N]")
            ax[1].set_xlabel("turns of boom remaining on reel [-]")

            ax[1].set_ylim(ylim_force)
            ax[1].set_xlim([(coiled_angle/(2*np.pi)).max(), 0.5])

            ax[1].legend(loc="best", fontsize="small", ncols=3)

        fig.set_figwidth(fig_width)
        fig.set_figheight(fig_height)
        fig.tight_layout()
        fig.savefig(f"{plot_dir}/{plot_name}_{sys.argv[2]}.png",
                    dpi=300,
                    format="png")

    except IndexError:
        raise ValueError("Specify a configuration to plot, such as 'config1'.")

elif sys.argv[1] == "plot_return_loss":

    # config
    directory = "return_loss"
    plot_dir = "plot-dory-paper"
    plot_name = "return_loss"

    fig_width = 8
    fig_height = 2

    xlim = [154, 170]
    ylim = [-16, -10]

    filenames = [file.name for file in os.scandir(directory)]

    filenames = sorted(filenames)

    fig, ax = plt.subplots(1,1)

    for index in range(0, len(filenames)):

        data = pd.read_csv(open(f"{directory}/{filenames[index]}"),
                           delimiter=",")

        data = data.sort_values(by="x")
        radius = filenames[index].removesuffix('.csv').removeprefix('DORY-tapespring-antenna_r')
        radius = int(radius)

        # plot torque
        ax.plot(data["x"], data["y"],
                color=list(seesat_colours.values())[index],
                label=fr"$r$ = {radius} [mm]",
                linewidth=1)

    ax.set_xlabel(r"frequency $f$ [MHz]")
    ax.set_ylabel("return loss [dB]")

    ax.set_xlim(xlim)
    ax.set_ylim(ylim)

    ax.legend(loc="best", fontsize="small")

    fig.set_figwidth(fig_width)
    fig.set_figheight(fig_height)
    fig.tight_layout()
    fig.savefig(f"{plot_dir}/{plot_name}.png",
                dpi=300,
                format="png")