"""

Calculations for linear springs.

"""


class LinearSpring():

    def __init__(self, stiffness, initial_deformation):
        self.stiffness = stiffness
        self.initial_deformation = initial_deformation

    def force(self, deformation):
        return (self.initial_deformation + deformation) * self.stiffness
