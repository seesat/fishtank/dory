"""

This file adds a number of utilities to be used for geometrical caluclations.

"""

import numpy as np

def area_circ(outer_diameter, inner_diameter):
    """
    Returns the area of a (hollow) circle.
    :param outer_diameter:  outer diameter of the circle
    :param inner_diameter:  inner diameter of the circle
    :return:                area of the circle
    """
    return 0.25 * np.pi * (outer_diameter**2 - inner_diameter**2)


def amoi_circle(outer_diameter, inner_diameter):
    """
    Returns the area moment of inertia of a (hollow) circle
    :param outer_diameter:  outer diameter of the circle
    :param inner_diameter:  inner diameter of the circle
    :return:                area moment of inertia of the circle
    """
    return np.pi / 64 * (outer_diameter ** 4 - inner_diameter ** 4)


def area_arc(outer_diameter, inner_diameter, polar_0, polar_1):
    """
    Returns the area of a circular arc with thickness
    `outer_diameter`/2 - `inner_diameter`/2
    :param outer_diameter:  outer diameter of the arc
    :param inner_diameter:  inner diameter of the arc
    :param polar_0:         polar angle at which the arc begins
    :param polar_1:         polar angle at which the arc ends
    :return:                area of the arc
    """
    return np.pi/4 * (outer_diameter**2 - inner_diameter**2) * (polar_1-polar_0)


def amoi_arc(outer_diameter, inner_diameter, polar_0, polar_1):
    """
    Return the area moment of inertia of a circular arc with thickness
    `outer_diameter`/2 - `inner_diameter`/2
    :param outer_diameter:  outer diameter of the arc
    :param inner_diameter:  inner diameter of the arc
    :param polar_0:         polar angle at which the arc begins
    :param polar_1:         polar angle at which the arc ends
    :return:                area moment of inertia of the arc
    """
    return 1/8 * (polar_1-polar_0 +
                  np.sin(polar_0)*np.cos(polar_0) -
                  np.sin(polar_1)*np.cos(polar_1)) * \
        1/16*(outer_diameter**4-inner_diameter**4)

def coa_arc(outer_diameter, inner_diameter, polar_0, polar_1):
    """
    Returns the centre of area of a circular arc in the (x,y) coordinate system.
    :param outer_diameter:  outer diameter of the arc
    :param inner_diameter:  inner diameter of the arc
    :param polar_0:         polar angle at which the arc begins
    :param polar_1:         polar angle at which the arc ends
    :return: position of in order (x,y)
    """

    x_cog = 1/3 * (outer_diameter ** 3 - inner_diameter ** 3) * \
        (np.sin(polar_1) - np.sin(polar_0)) / \
        (outer_diameter ** 2 - inner_diameter ** 2) / \
        (polar_1 - polar_0)

    y_cog = -1/3 * (outer_diameter**3 - inner_diameter**3) * \
        (np.cos(polar_1)-np.cos(polar_0)) / \
        (outer_diameter**2 - inner_diameter**2) / \
        (polar_1 - polar_0)

    return x_cog, y_cog
