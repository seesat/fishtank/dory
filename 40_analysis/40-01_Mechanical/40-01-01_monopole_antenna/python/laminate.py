"""

This file adds a number of utilities to be used for laminate theory
calculations.

"""

import numpy as np


class LaminateTransform:

    @classmethod
    def transform_local_to_global(cls, theta):
        T11 = np.cos(theta)**2
        T12 = np.sin(theta)**2
        T13 = - 2 * np.sin(theta) * np.cos(theta)
        T21 = np.sin(theta)**2
        T22 = np.cos(theta)**2
        T23 = 2 * np.sin(theta) * np.cos(theta)
        T31 = np.sin(theta) * np.cos(theta)
        T32 = - np.sin(theta) * np.cos(theta)
        T33 = np.cos(theta)**2 - np.sin(theta)**2

        T = np.array([[T11, T12, T13], [T21, T22, T23], [T31, T32, T33]])

        return T

    @classmethod
    def transform_global_to_local(cls, theta):
        T11 = np.cos(theta)**2
        T12 = np.sin(theta)**2
        T13 = 2 * np.sin(theta) * np.cos(theta)
        T21 = np.sin(theta)**2
        T22 = np.cos(theta)**2
        T23 = - 2 * np.sin(theta) * np.cos(theta)
        T31 = - np.sin(theta) * np.cos(theta)
        T32 = np.sin(theta) * np.cos(theta)
        T33 = np.cos(theta)**2 - np.sin(theta)**2

        T = np.array([[T11, T12, T13], [T21, T22, T23], [T31, T32, T33]])

        return T

    @classmethod
    def transform_tensor_to_engineering_3(cls):
        return np.array([[1, 0, 0], [0, 1, 0], [0, 0, 2]])

    @classmethod
    def transform_tensor_to_engineering_6(cls):
        transf_zero = np.concatenate(
            [LaminateTransform.transform_tensor_to_engineering_3(),
             np.zeros([3, 3])], axis=1)

        zero_transf = np.concatenate(
            [np.zeros([3, 3])],
            LaminateTransform.transform_tensor_to_engineering_3(), axis=1)

        return np.concatenate([transf_zero, zero_transf], axis=0)

    @classmethod
    def transform_engineering_to_tensor_3(cls):
        return np.array([[1, 0, 0], [0, 1, 0], [0, 0, 0.5]])

    @classmethod
    def transform_engineering_to_tensor_6(cls):
        transf_zero = np.concatenate(
            [LaminateTransform.transform_engineering_to_tensor_3(),
             np.zeros([3, 3])], axis=1)

        zero_transf = np.concatenate(
            [np.zeros([3, 3])],
            LaminateTransform.transform_engineering_to_tensor_3(), axis=1)

        return np.concatenate([transf_zero, zero_transf], axis=0)


class DeformationState:

    def __init__(self, strain, curvature):
        """
        Initializes the deformation state.
        :param strain: 3x1 vector of engineering strain (eps_x,eps_y,gamma_xy)
        :param curvature: 3x1 vector of curvature (k_x,k_y,k_xy)
        """

        self.__strain = strain
        self.__curvature = curvature
        self.__build_deformation_vector()

    @property
    def strain(self):
        return self.__strain

    @strain.setter
    def strain(self, new_strain):
        self.__strain = new_strain
        if self.curvature is not None:
            self.__build_deformation_vector()

    @property
    def curvature(self):
        return self.__curvature

    @curvature.setter
    def curvature(self, new_curvature):
        self.__curvature = new_curvature
        if self.strain is not None:
            self.__build_deformation_vector()

    @property
    def deformation_vector(self):
        return self.__deformation_vector

    @deformation_vector.setter
    def deformation_vector(self, x):
        raise UserWarning("It is not allowed to set the deformation vector "
                          "manually. Please set the strain and curvature "
                          "vectors. The deformation vector will be adjusted "
                          "automatically.")

    def __build_deformation_vector(self):
        self.__deformation_vector = \
            np.concatenate((self.strain, self.curvature), axis=0)

    @classmethod
    def __transform_engineering_vector_to_global_3(cls, engineering_vector,
                                                   theta):
        global_vector = \
            np.matmul(LaminateTransform.transform_engineering_to_tensor_3(),
                      engineering_vector)

        global_vector = \
            np.matmul(LaminateTransform.transform_local_to_global(theta),
                      global_vector)

        global_vector = \
            np.matmul(LaminateTransform.transform_tensor_to_engineering_3(),
                      global_vector)

        return global_vector

    def global_strain(self, theta):
        return self.__transform_engineering_vector_to_global_3(self.strain,
                                                               theta)

    def global_curvature(self, theta):
        return self.__transform_engineering_vector_to_global_3(self.curvature,
                                                               theta)

    def global_deformation(self, theta):
        return np.concatenate([self.global_strain(theta),
                               self.global_curvature(theta)],
                              axis=0)


class OrthotropicLamina:

    def __init__(self, E1, E2, G12, v12, v21, theta,
                 initial_deformation: DeformationState):
        """
        Constructor for an OrthotropicLamina object
        :param E1:      stiffness along main fiber direction
        :param E2:      stiffness perpendicular to main fiber direction
        :param G12:     shear modulus of fiber-matrix composite
        :param v12:     poisson ratio along main fiber direction
        :param v21:     poisson ratio perpendicular to main fiber direction
        :param theta:   angle between main fiber direction and global x-axis
        """

        self.E1 = E1
        self.E2 = E2
        self.G12 = G12
        self.v12 = v12
        self.v21 = v21
        self.theta = theta
        self.initial_deformation = initial_deformation

    def Q(self):
        """
        Determines the stiffness matrix in the fiber coordinate system.
        All stresses are defined as tensorial.
        :return: local stiffness matrix
        """
        Q11 = self.E1/(1-self.v12*self.v21)
        Q12 = self.v12*self.E2/(1-self.v12*self.v21)
        Q22 = self.E2/(1-self.v12*self.v21)
        Q66 = self.G12

        Q = np.array([[Q11, Q12, 0], [Q12, Q22, 0], [0, 0, Q66]])

        return Q

    def Q_global(self):
        """
        Determines the stiffness matrix in the global coordinate system.
        The resulting matrix works with engineering strains.
        :return: global stiffness matrix
        """
        T = LaminateTransform.transform_local_to_global(self.theta)
        T_inv = LaminateTransform.transform_global_to_local(self.theta)

        Q_global = \
            np.matmul(LaminateTransform.transform_tensor_to_engineering_3(),
                      T_inv)
        Q_global = np.matmul(self.Q(), Q_global)
        Q_global = np.matmul(T, Q_global)
        Q_global = \
            np.matmul(Q_global,
                      LaminateTransform.transform_engineering_to_tensor_3())

        return Q_global


class OrthotropicLaminate:

    def __init__(self, lamina, distances, deformation: DeformationState):
        """
        Constructor for an OrthotropicLaminate object
        :param lamina:      list of OrthotropicLamina objects
        :param distances:   list of floats that define the distance of the
        lamina separation planes from the defined center separation plane; size
        must be one larger than that of the lamina list
        :param deformation:
        """

        assert len(distances) - len(lamina) == 1

        self.lamina = lamina
        self.distances = distances
        self.deformation = deformation

        self.stiffness_matrix = self.__calc_stiffness_matrix()

    @property
    def stiffness_matrix(self):
        return self.__stiffness_matrix

    @stiffness_matrix.setter
    def stiffness_matrix(self, full_stiffness_matrix):
        self.__stiffness_matrix = full_stiffness_matrix

    def __A_i(self, lamina_index):

        A_i = self.lamina[lamina_index].Q_global() * \
            np.abs(self.distances[lamina_index+1] -
                   self.distances[lamina_index])

        return A_i

    def __B_i(self, lamina_index):

        B_i = 0.5 * self.lamina[lamina_index].Q_global() * \
            np.abs(self.distances[lamina_index+1]**2 -
                   self.distances[lamina_index]**2)

        return B_i

    def __D_i(self, lamina_index):

        D_i = 1/3 * self.lamina[lamina_index].Q_global() * \
            np.abs(self.distances[lamina_index+1]**3 -
                   self.distances[lamina_index]**3)

        return D_i

    def __stiffness_matrix_i(self, lamina_index):
        AB_i = np.concatenate((self.__A_i(lamina_index),
                               self.__B_i(lamina_index)), axis=0)
        BD_i = np.concatenate((self.__B_i(lamina_index),
                               self.__D_i(lamina_index)), axis=0)

        stiffness_matrix_i = np.concatenate((AB_i, BD_i), axis=1)

        return stiffness_matrix_i

    def A(self):
        A = np.zeros((3, 3))

        for i in range(0, len(self.lamina)):
            A += self.__A_i(i)

        return A

    def B(self):
        B = np.zeros((3, 3))

        for i in range(0, len(self.lamina)):
            B += self.__B_i(i)

        return B

    def D(self):
        D = np.zeros((3, 3))

        for i in range(0, len(self.lamina)):
            D += self.__D_i(i)

        return D

    def __calc_stiffness_matrix(self):
        AB = np.concatenate((self.A(), self.B()), axis=0)
        BD = np.concatenate((self.B(), self.D()), axis=0)

        stiffness_matrix = np.concatenate((AB, BD), axis=1)

        return stiffness_matrix

    def enforced_eng_strain_from_curvature(self, lamina_index):
        eng_strains_from_curvature = \
            self.deformation.curvature * 0.5 * \
            (self.distances[lamina_index] + self.distances[lamina_index+1])

        return eng_strains_from_curvature

    def total_deformation_i(self, lamina_index):
        """
        Returns the total engineering deformations
        (eps_x, eps_y, gamma_xy, k_x, k_y, k_xy)^T
        """

        total_deformation = \
            self.lamina[lamina_index].initial_deformation.global_deformation(
                self.lamina[lamina_index].theta) + \
            self.deformation.deformation_vector

        return total_deformation

    def energy_density(self):
        u_s = 0

        for i in range(0, len(self.lamina)):

            total_lamina_deformation = self.total_deformation_i(i)

            induced_deformation = \
                np.concatenate([self.enforced_eng_strain_from_curvature(i),
                               np.ones([3, 1])], axis=0)

            u_i = np.matmul(self.__stiffness_matrix_i(i),
                            total_lamina_deformation)

            u_i = \
                0.5 * np.matmul((total_lamina_deformation).T, u_i)

            u_s += u_i.item()

        return u_s


if __name__ == "__main__":
    import sys

    if sys.argv[1] == "test_stiffness":
        neutral_deformation = DeformationState(np.array([[0], [0], [0]]),
                                               np.array([[0], [0], [0]]))

        E1 = 137.964e+9
        E2 = 8.970079e+9
        G12 = 6.90165e+9
        v12 = 0.3

        l1 = OrthotropicLamina(E1=E1, E2=E2, G12=G12, v12=v12,
                               v21=v12 * E2 / E1, theta=0,
                               initial_deformation=neutral_deformation)
        l2 = OrthotropicLamina(E1=E1, E2=E2, G12=G12, v12=v12,
                               v21=v12 * E2 / E1, theta=np.pi / 4,
                               initial_deformation=neutral_deformation)
        l3 = OrthotropicLamina(E1=E1, E2=E2, G12=G12, v12=v12,
                               v21=v12 * E2 / E1, theta=np.pi / 4,
                               initial_deformation=neutral_deformation)
        l4 = OrthotropicLamina(E1=E1, E2=E2, G12=G12, v12=v12,
                               v21=v12 * E2 / E1, theta=0,
                               initial_deformation=neutral_deformation)

        h = [-2.54e-4, -1.27e-4, 0, +1.27e-4, +2.54e-4]

        lamina = [l1, l2, l3, l4]

        laminate = OrthotropicLaminate(lamina, h, neutral_deformation)

        test_buffer = laminate.stiffness_matrix/175.12

        print("Done!")

    elif sys.argv[1] == "test_energy_density":

        import matplotlib.pyplot as plt

        deformation_1 = DeformationState(np.array([[0], [0], [0]]),
                                         np.array([[-62.5], [0], [0]]))

        deformation_2 = DeformationState(np.array([[0], [0], [0]]),
                                         np.array([[62.5], [0], [0]]))

        E1 = 125e+9
        E2 = 7.5e+9
        G12 = 6.3e+9
        v12 = 0.33

        l1 = OrthotropicLamina(E1=E1, E2=E2, G12=G12, v12=v12,
                               v21=v12 * E2 / E1, theta=0,
                               initial_deformation=deformation_1)
        l2 = OrthotropicLamina(E1=E1, E2=E2, G12=G12, v12=v12,
                               v21=v12 * E2 / E1, theta=np.pi / 2,
                               initial_deformation=deformation_2)

        h = [-1.3e-4, 0, +1.3e-4]

        lamina = [l1, l2]

        k_e = np.linspace(-70, 70, 100)

        theta_e = np.linspace(-95/180*np.pi, 95/180*np.pi, 500)

        k_e_v, theta_e_v = np.meshgrid(k_e, theta_e)

        energy_densities = []

        neutral_deformation = DeformationState(np.array([[0], [0], [0]]),
                                               np.array([[0], [0], [0]]))

        laminate = OrthotropicLaminate(lamina, h, neutral_deformation)

        for (ke, thet) in zip(k_e_v.flatten(), theta_e_v.flatten()):

            local_enforced_curvature = np.array([[0, ke, 0]]).T



            local_enforced_deformation = \
                DeformationState(np.array([[0], [0], [0]]),
                                 local_enforced_curvature)

            local_enforced_deformation.curvature = \
                local_enforced_deformation.global_curvature(thet)

            laminate.deformation = local_enforced_deformation

            energy_densities.append(laminate.energy_density())

        energy_densities = np.array(energy_densities).reshape(k_e_v.shape)

        fig, ax = plt.subplots()

        c = ax.contour(k_e, theta_e, energy_densities, 100)
        ax.clabel(c, fmt="%2.1f", fontsize=10)
        fig.colorbar(c)
        plt.show()

        min_index = np.argmin(energy_densities)
        k_e_v_min = k_e_v.flatten()[min_index]
        theta_min = theta_e_v.flatten()[min_index]
        print(f"Minimum energy density at k={k_e_v_min} and theta={theta_min}")
        print("Resulting curvature vector:")
        print(np.matmul(
            LaminateTransform.transform_local_to_global(theta_min),
            np.array([[0, k_e_v_min, 0]]).T))

    elif sys.argv[1] == "test_energy_density_2":

        import matplotlib.pyplot as plt

        k_i = -157.5
        theta_i = -np.pi/4

        curvature = k_i * np.array([[np.sin(theta_i) ** 2],
                                    [np.cos(theta_i) ** 2],
                                    [-2 * np.sin(theta_i) * np.cos(theta_i)]])

        deformation_1 = DeformationState(np.array([[0], [0], [0]]),
                                         curvature)

        E1 = 53.2e+9
        E2 = 53.2e+9
        G12 = 3.86e+9
        v12 = 0.059

        l1 = OrthotropicLamina(E1=E1, E2=E2, G12=G12, v12=v12,
                               v21=v12 * E2 / E1, theta=np.pi / 4,
                               initial_deformation=deformation_1)

        h = [+1.27e-4, -1.27e-4]

        lamina = [l1]

        k_e = np.linspace(0, 200, 100)

        theta_e = np.linspace(-0.5*np.pi, 0.5*np.pi, 500)

        k_e_v, theta_e_v = np.meshgrid(k_e, theta_e)

        energy_densities = []

        neutral_deformation = DeformationState(np.array([[0], [0], [0]]),
                                               np.array([[0], [0], [0]]))

        laminate = OrthotropicLaminate(lamina, h, neutral_deformation)

        for (ke, thet) in zip(k_e_v.flatten(), theta_e_v.flatten()):

            local_enforced_curvature = np.array([[0, ke, 0]]).T

            local_enforced_deformation = \
                DeformationState(np.array([[0], [0], [0]]),
                                 local_enforced_curvature)

            local_enforced_deformation.curvature = \
                local_enforced_deformation.global_curvature(thet)

            laminate.deformation = local_enforced_deformation

            energy_densities.append(laminate.energy_density())

        energy_densities = np.array(energy_densities).reshape(k_e_v.shape)

        fig, ax = plt.subplots()

        c = ax.contour(k_e, theta_e, energy_densities, 50)
        ax.clabel(c, fmt="%2.1f", fontsize=10)
        fig.colorbar(c)
        plt.show()

        min_index = np.argmin(energy_densities)
        k_e_v_min = k_e_v.flatten()[min_index]
        theta_min = theta_e_v.flatten()[min_index]
        print(f"Minimum energy density at k={k_e_v_min} and theta={theta_min}")
        print("Resulting curvature vector:")
        print(np.matmul(
            LaminateTransform.transform_local_to_global(theta_min),
            np.array([[0, k_e_v_min, 0]]).T))

    elif sys.argv[1] == "test_energy_density_3":

        import matplotlib.pyplot as plt

        k_i = -157.5
        theta_i = -np.pi/4

        curvature = k_i * np.array([[np.sin(theta_i) ** 2],
                                    [np.cos(theta_i) ** 2],
                                    [-2 * np.sin(theta_i) * np.cos(theta_i)]])

        deformation_1 = DeformationState(np.array([[0], [0], [0]]),
                                         curvature)

        E1 = 53.2e+9
        E2 = 53.2e+9
        G12 = 3.86e+9
        v12 = 0.059

        l1 = OrthotropicLamina(E1=E1, E2=E2, G12=G12, v12=v12,
                               v21=v12 * E2 / E1, theta=np.pi / 4,
                               initial_deformation=deformation_1)

        h = [+1.27e-4, -1.27e-4]

        lamina = [l1]

        k_e = np.linspace(0, 200, 100)

        theta_e = np.linspace(-0.5*np.pi, 0.5*np.pi, 500)

        k_e_v, theta_e_v = np.meshgrid(k_e, theta_e)

        energy_densities = []

        neutral_deformation = DeformationState(np.array([[0], [0], [0]]),
                                               np.array([[0], [0], [0]]))

        laminate = OrthotropicLaminate(lamina, h, neutral_deformation)

        for (ke, thet) in zip(k_e_v.flatten(), theta_e_v.flatten()):

            local_enforced_curvature = np.array([[0, ke, 0]]).T

            local_enforced_deformation = \
                DeformationState(np.array([[0], [0], [0]]),
                                 local_enforced_curvature)

            local_enforced_deformation.curvature = \
                local_enforced_deformation.global_curvature(thet)

            laminate.deformation = local_enforced_deformation

            energy_densities.append(laminate.energy_density())

        energy_densities = np.array(energy_densities).reshape(k_e_v.shape)

        fig, ax = plt.subplots()

        c = ax.contour(k_e, theta_e, energy_densities, 50)
        ax.clabel(c, fmt="%2.1f", fontsize=10)
        fig.colorbar(c)
        plt.show()

        min_index = np.argmin(energy_densities)
        k_e_v_min = k_e_v.flatten()[min_index]
        theta_min = theta_e_v.flatten()[min_index]
        print(f"Minimum energy density at k={k_e_v_min} and theta={theta_min}")
        print("Resulting curvature vector:")
        print(np.matmul(
            LaminateTransform.transform_local_to_global(theta_min),
            np.array([[0, k_e_v_min, 0]]).T))
