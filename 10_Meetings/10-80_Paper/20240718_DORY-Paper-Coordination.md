# 2024-07-03 DORY Paper Task Planning

## Dates

10.09. submission of co-authors

20.09. paper deadline

30.09. - 02.10. DLRK Hamburg


## Schedule

- 12 weeks

- 03.07. - 20:00 - fixed time slot for defining content, planning schedule and making work packages should be defined

- 30.08. - finalization of work
- 06.09. - first draft


## Availability

Paul:
- 24.08. - 30.08. - not available

Lena:
- up to 08.08.: DLR Design Challenge -> very limited availability
- from 12.08.: available
- September: availability pending success at DLR DC

Martin:
- 01.07. - 05.07. -> not available
- 21.08. - 03.09. -> not available

Leo:
- up to 16.09. -> not available
- from 16.09. -> available

Tom:
- 01.07. - 14.07. -> not available
- 
- from 01.09. -> not available

Pascal:
- likely not available


## Content

- simulation:
	- pure simulation is easily doable
	- good definition of simulation goal is paramount
	- analysis of results is most of the work

- Content:
	- 0.5 pages -> Introduction + Problem
		- current state of IUU fishing
		- current methods of detecting IUU fishing
		- space observation for AIS tracking
		- advantage of cubesats -> modularity, etc. -> *simple and cheap*
	- 0.5 pages -> Preliminary Link Budget
	- 0.5 pages -> Requirements
		- Antenna Gain -> result of link Budget
		- Physical limitations
		- Return loss -10 dB
		- observation angle range
		- mechanical requirements
	- 3 pages -> Communications
		- 1.5 Studienarbeit
			- Preliminary antenna design
				- short description of infeasibility of parabolic and horn antennas
				- short description of best helix result
			- end result for dipole antenna
				- return loss over frequency
				- directional antenna gain
		- 1.5 new
			- final antenna Design -> incl. mechanical design with optimised length
				- return loss over frequency
				- directional antenna gain
			- comparison lambda/2 vs. lambda total length 
				- two models
			- simple optimisation of parameters for antenna performance
				- two simulations
				- trade-off antenna vs. mechanical
			- one simulation for other frequency
	- 3 pages -> Mechanical
		- 3 new
			- Design Justification
			- Concept description incl. image
				- approximate physical dimensions
				- materials
				- images!
			- Description of mechanism
				- comparison stowed vs. deployed
				- transient behaviour during deployment
				- forces, energy, power etc.
				- images!
				- deployment actuation and locking
				- fault tolerance
			- Material Trade-off
			- dynamic behaviour of boom
		
		
## Preliminary work packages

Mechanical Design:

- Design Justification -> Lena
	- one concept, justification of why
- Concept description incl. image -> Martin, Lena
	- approximate physical dimensions
	- materials
	- images!
- Description of mechanism
	- comparison stowed vs. deployed -> Martin
	- (transient behaviour during deployment) -> Paul
	- forces, energy, power etc. -> Paul
	- images!
	- deployment actuation and locking -> Lena
	- (fault tolerance -> Martin, Lena)
- Material Trade-off
	- likely dominated by mechanical constraints -> Lena, Paul
- dynamic behaviour of boom -> Paul

Antenna Design:

- condense information of Studienarbeit -> Leo
	- Introduction
	- link budget
	- preliminary antenna design

- Simulation
	- plan simulation -> Tom + other people
		- which parameters
		- scope (how detailed, parameter variations, optimisation?)
			- final antenna Design
				- return loss over frequency
				- directional antenna gain
			- comparison lambda/4 vs. lambda/2
				- two models
			- simple optimisation of parameters
				- two simulations
				- (material trade-off)
			- one simulation for other frequency
		- which data to produce
	- excute simulation -> Tom
		- build model
		- build parameter space
		- generate raw data
	- analyze results
		- prepare raw data (pre-processing) -> Tom
		- in-depth analysis (calculate performance parameters, visualize)
		- write documentation
