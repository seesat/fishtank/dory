# 2024-06-19 DORY Paper Kick-Off

## Dates

27.06. confirmation

10.09. submission of co-authors

20.09. paper deadline

30.09. - 02.10. DLRK Hamburg


## Schedule

- 12 weeks

- 03.07. - 20:00 - fixed time slot for defining content, planning schedule and making work packages should be defined

- 30.08. - finalization of work
- 06.09. - first draft


## Availability

Paul:
- 24.08. - 30.08. - not available

Lena:
- up to 08.08.: DLR Design Challenge -> very limited availability
- from 12.08.: available
- September: availability pending success at DLR DC

Martin:
- 01.07. - 05.07. -> not available
- 21.08. - 03.09. -> not available

Leo:
- up to 16.09. -> not available
- from 16.09. -> available

Tom:
- 01.07. - 14.07. -> not available
- 
- from 01.09. -> not available


## Content

- simulation:
	- pure simulation is easily doable
	- good definition of simulation goal is paramount
	- analysis of results is most of the work

- Content:
	- 0.5 pages -> Introduction + Problem
	- 0.5 pages -> Requirements
	- 3 pages -> Communications
		- 1.5 Studienarbeit
		- 1.5 new
	- 3 pages -> Mechanical
		- 3 new
		
## Preliminary work packages

- condense information of Studienarbeit -> Leo
	- Introduction
	- link budget
	- preliminary antenna design

- simulation
	- plan simulation -> Tom + other people
		- which parameters
		- scope (how detailed, parameter variations, optimisation?)
			- final antenna Design
				- return loss over frequency
				- directional antenna gain
			- comparison lambda/4 vs. lambda/2
				- two models
			- simple optimisation of parameters
				- two simulations
			- one simulation for other frequency
		- which data to produce
	- excute simulation -> Tom
		- build model
		- build parameter space
		- generate raw data
	- analyze results
		- prepare raw data (pre-processing) -> Tom
		- in-depth analysis (calculate performance parameters, visualize)
		- write documentation
