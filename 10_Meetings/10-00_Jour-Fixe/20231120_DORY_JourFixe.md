# 2023-11-2023 DORY Jour Fixe

## Attendees

- Pascal
- Leo
- Tom
- Paul

## DORY Studienarbeit

1. Tooling

2023-11-14 AI-01: Permanent License for FEKO
- NTR

2023-11-14 AI-02: Test FEK Account
- NTR

2. Responsibilities
- NTR

3. Literature
- Pascal, Leo, Tom pooling resources to to literature studies for poster
- First info added to LaTeX
- Leo: AIS and standard fully summarized, five sources researched; only missing some information on link budget
	- Open question: How to describe necessary antenna characteristics for AIS (i.e. directional characteristics, emission power etc.)
- Tom: researching antenna technologies, eight types selected for further study that may be applicable; further research into characteristics
- Pascal: researched reference missions; Larpan papers on IEEE found with micro-satellite missions receiving AIS signals; minimum 2dBI identified as minimum antenna gain; patch antenna might also be interesting based on research results

- Paul: Further interesting mission concept: 3Cat-4 from UPC https://nanosatlab.upc.edu/en/missions-and-projects/3cat-4

- Tom and Pascal will research link budget; idea to write Matlab script for determining budget depending on height and signal angle of incidence
- Paul: Space Mission Analysis and Design might be useful for determining damping factors and other influences on link budget

- *AI-01* Tom, Leo, Pascal: Discuss which antenna types to analyze
- *AI-02* Pascal: Identify design drivers based on reference missions
- *AI-03* Leo: Clear up way to research characteristics with Dennis

4. Poster
- NTR
- Preliminary content:
	- AIS standard
	- Potential antenna types
	- Reference Missions
- Due Date: 2023-11-29, 23:59

5. Exchange of Information
- General:
	- Tom: How to specificy impedance of antenna? -> working assumption: signal circuit of 50 Ohms, antenna should adapt impedance to free-field impedance in vacuum
	*AI-04*: Tom: Cross-check impendance requirements with Dennis
	
- NEMO:
	- link budget depending on height -> graph showing necessary budget - depending on height useful for NEMO mission design


6. Final Studienarbeit document
- Combined two AIS chapters
- separated conclusion into three distinct chapters
- further minor changes to structure
- adjusted some chapter titles
- Changed language to German
- visual changes

- *AI-05*: Leo: Dicuss changes to document structure with Dennis

7. Further Meetings

## Actions

- *AI-05*: Tom, Leo, Pascal: Discuss which antenna types to analyze
- *AI-02*: Pascal: Identify design drivers based on reference missions
- *AI-03*: Leo: Clear up way to research characteristics with Dennis (scientific sources or only market research etc.)
- *AI-04*: Tom: Cross-check impendance requirements with Dennis
- *AI-05*: Leo: Dicuss changes to document structure with Dennis