# 2023-11-29 DORY Jour Fixe

## Attendees

- Pascal
- Leo
- Tom
- Dennis
- Paul

## DORY Studienarbeit

1. Tooling

2023-11-14 *AI-01*: Permanent License for FEKO
- current license is only valid for single account

2023-11-14 *AI-02*: Test FEKO Account -> done
- Tested on di-pole antenna for content of poster

- Dennis: please write code with parameter

2. Responsibilities
- NTR

3. Literature
- Pascal, Leo, Tom pooling resources to to literature studies for poster
- First info added to LaTeX
- Leo: AIS and standard fully summarized, additional sources implemented
	- Open question: How to describe necessary antenna characteristics for AIS (i.e. directional characteristics, emission power etc.)
	- current issue: standard is well documented, antenna requirements are not defined in detail (only 12.5W defined in standard for horizontal direction) -> important for link budget; 0dB antenna gain and 12.5W is often used
	- 
	- class A system is applicable for all vessels to be monitored
	- rough estimate for orbit height -> 300...500km -> useful for link budget
	- weather shall be included as parameter in link budget estimation -> use exiting models (ITU, etc.)
	
	*AI-01* Leo: give assumption of using Class A system to NEMO team
	
- Tom: researching antenna technologies, eight types selected for further study that may be applicable; further research into characteristics
	- Question: Should antenna types be reduced to those already proven in satellite? -> Paul: Yes, please focus on proven system, miniaturization is
	- Likely rod antennas (mono-/dipole)
	- Parabolic antennas likely not doable at this size, but still interesting
	- Helix antenna as a good option as well
- Pascal: researched reference missions; Larpan papers on IEEE found with micro-satellite missions receiving AIS signals; minimum 2dBI identified as minimum antenna gain; patch antenna might also be interesting based on research results
	- 3cat-4 -> lambda/4 monopole antenna
	- nanosat database -> a number of cubesats used for collecting AIS data; e.g. yusat-1
	- Larpan-A4 satellite

- _Leo_ and Pascal will research link budget; idea to write Matlab script for determining budget depending on height and signal angle of incidence
	- started researching
- Paul: Space Mission Analysis and Design might be useful for determining damping factors and other influences on link budget

4. Poster
- done
- Feedback:
	- expand problem description with mention of cubesat in low earth orbit
	- add some information on tooling and previous activity
- Preliminary content:
	- AIS standard
	- Potential antenna types
	- Reference Missions
- Due Date: 2023-11-29, 23:59

## Actions

- *AI-01*: give assumption of using Class A system to NEMO team