# 2024-10-08 DORY Studienarbeit - Kick-Off

## Attendees
- Lena
- Martin
- Paul

## References
- List of reference documents: https://nc.seesat.eu/f/111088
- DORY DLRK2024 Paper: https://nc.seesat.eu/f/62269

## Studienarbeit - Planning

### Potential Work Packages

- Systems Engineering:
	- Definition of mission requirements
	- Definition of Structural Requirements
	- Definition of test requirements
	- Overall system design
	- Definition of test cases (vibration, shock, deployment etc.)

- Mechanical Design:
	- design of deployment mechanism
	- design of arrest mechanism for deployment
	- structural design of antenna housing
	- detailed design of antenna boom
	- design of test stand for first tests

- Mechanical Analysis:
	- FE analysis of mechanical design for static and dynamic loads
	- FE analysis of mechanical design with regard to fundamental modes

- Manufacturing, Assembly, Integration & Test:
	- manufacturing of first tapespring prototypes (CFRP or metal)
	- potentially finding sponsors for parts
	- planning of integration steps
	- planning of test steps
	
- Electrical and Functional design:
	- rough concept design for AIS signal distribution
	- interface definition for AIS data
	- definition of housekeeping sensors
	- control concept for active mechanism components
	- optional: first high-level test concept (i.e. test flow etc.)
	
### Distribution

- Lena & Martin:
	- Title: "Mechanische Auslegung einer verstaubaren Cubesat-Antenne"
	- Content:
		- Systems Engineering
		- Mechanical Design
		- Mechanical Analysis (initial)
	
- Mica:
	- Title: TBD
	- Content:
		- Definition of test cases (vibration, shock, deployment etc.)
		- design of test stand for first tests
		- manufacturing of first tapespring prototypes (CFRP or metal)
		- potentially finding sponsors for parts
		- planning of integration steps
		- planning of test steps

- Natalie:
	- Title: "Conceptual electrical and functional system design for a stowable cubesat antenna"
	- Content:
		- Electrical and Functional design:
			- rough concept design for AIS signal distribution
			- interface definition for AIS data
			- definition of housekeeping sensors
			- control concept for active mechanism components
			- optional: first high-level test concept (i.e. test flow etc.)

	
### Dates

- 2024-10-09: Deadline Anmeldung Studienarbeit TEN/TEK
- 2024-10-11: Deadline Anmeldung Studienarbeit TLR/TLE

## Next Points

- Potentially add some functional design aspects to Natalie's Studienarbeit -> Paul