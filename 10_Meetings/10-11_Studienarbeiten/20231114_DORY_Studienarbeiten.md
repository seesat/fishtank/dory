# 2023-11-14 DORY Studienarbeiten

## Teilnehmer
- Dennis
- Tom
- Pascal

## DORY Studienarbeit

1. Tooling

**FEKO Inbetriebnahme**
Pascal hat FEKO fertig installiert und die Lizenzn eingebunden. Als Lizenz wird der FEKO Account samt Passwort genutzt. Nach dem Meeting soll die Installation auf dem Laborrechner abgeschlossen werden.

AI-01 Dennis: Mit Al-Tameemi über dedizierte Lizenz für den SeeSat e.V. (Laborrechner) sprechen.
AI-02 Pascal / Tom: Ausprobieren ob die Anmeldung mit der Lizenz für Accounts gilt.
AI-03 Pascal: Installationsguide schreiben. (Anahng zur Studienarbeit) --> Wiki

**CAD (Siemens NX)**
Es gab noch keine Absprache mit dem Mechanical Team um die Schnittstelle mit denen abzustimmen. Aus den Minutes der Mechaniker geht hervor, dass sich für Siemens NX entschieden wurde. Abstimmungen notwendig.

AI-04 Pascal: Kontaktiert Paul/Lena/Dominik um die Schnittstellen abzuklären.

**Overleaf**
Die Jungs haben in der NextCloud ein Overleaf Template gefunden. Das ist nicht das aktuelle. Ansprechperson ist Antonius. Das aktuelle Template wird nach dem Workshop (morgen, 17:00) verteilt.

2. Zuständigkeiten
In der letzten Sitzung wurde die Struktur der Arbeit durchgesprochen. Es gab die Aufgabe die drei Themengebiete "Simulation", "System-Engineering - AIS", "Antennendesign" innerhalb des Teams aufzuteilen.

Leo:	System-Engineering
Pascal: Simulation
Tom:	Antennendesign

Klarstellung der Pakete
System Engineering: Verstehen vom AIS-Standard (+Space based AIS), Aufstellung vom Link-Budgets, Anforderungsanalyse
Simualiaton: Inbetriebnahme Toolchain + Durchführung/Auswertung der Simulationen
Antennendesign: Recherche zu Antennentypen, Design der Antenne, Unterstützung Anforderungsanalyse 

3. Stand Literatur Recherche
Recherche läuft. Ergebnisse werden in das Overleaf Projekt eingepflegt sobald der Zugang besteht.
Für das nächste Meeting soll eine Liste mit den angeschauten Quellen vorbereitet werden.

4. Poster
Die Inhalte der Recherche sollen auf einem Poster für die Weihnachtsfeier am 06.12. aufbereitet werden. Für die Bestellung muss das Posterdesign bis 23:59 am 29.11 bei Dennis sein.


5. Austausch mit NEMO
Am Ende des Meetings kamen die NEMO Leute ins RITZ. Es wurde sich noch mit denen ausgetauscht.

6. Zukünftige Meetings (auch mit NEMO)
Dieses Jahr sollen/wollen wir noch drei Meetings machen.

Mo, 20.11. 18:00 - 19:00 https://vereinonline.org/SeeSat/?veranstaltung=78019
Di, 28.11. 16:45 - 17:45 mit NEMO (https://vereinonline.org/SeeSat/?veranstaltung=77917)
Fr, 08.12. 15:00 - 16:00 https://vereinonline.org/SeeSat/?veranstaltung=78020


## Actions
AI-01 Dennis: Mit Al-Tameemi über dedizierte Lizenz für den SeeSat e.V. (Laborrechner) sprechen.
AI-02 Pascal / Tom: Ausprobieren ob die Anmeldung mit der Lizenz für Accounts gilt.
AI-03 Pascal: Installationsguide schreiben. (Anahng zur Studienarbeit) --> Wiki
AI-04 Pascal: Kontaktiert Paul/Lena/Dominik um die Schnittstellen abzuklären.
