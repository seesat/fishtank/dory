# 2023-10-25 DORY Studienarbeiten Kick-Off

## Teilnehmer
- Dennis
- Paul
- Lena
- Leo
- Tom
- Pascal

## DORY Studienarbeit

0. Allgemein

- Bewertungsschema:	Eine Benotung für gesamte Arbeit gilt für alle

1. Zwischenstand bis Ende des Jahres / Semester:

- 20 Seiten ohne Verzeichnisse
- Struktur der Arbeit
- Einleitung
- Recherche-Ergebnisse
	- Stand der Technik (hauptsächlich Nachrichtentechnik, Usecase nur kurz erwähnen)
	- Verweise auf Literatur; unformatiert mit kurzem Inhalt in ein paar Sätzen
- erste Ideen und Gedanken

- Veränderung des Texts ist im weiteren Verlauf der Arbeit möglich
- Struktur und Arbeitspensum

- Poster-Session
	
2. Inhalte:

- Aufgabenstellung
	- ca. eine Seite
- Stand der Technik
	- u.a. AIS-Charakteristik
- Anforderungen
	- Link-Budget
	- Frequenzen
	- Leistung
- Konzepte
- Simulation & Toolchain
- Simulationsergebnisse
- Entscheidung für ein Konzept
	- Begründung, Trade-Off
	- Beschreibung der finalen Charakteristik
	
3. Bewertungskriterien von Dennis:

- Formatierung und durchgängige Verzeichnisse
- Angemessene Zusammenfassung anderer Erkenntnisse
- Präzise und zusammengefasste Formulierungen
- sinnvoller roter Faden und Gedankenstrom
- Verständlichkeit für nicht-involvierte Personen
- lediglich Bewertung der Arbeit

## Actions
- erster Ansatz für Struktur der Arbeit		-> Pascal, Tom, Leo, DUE: 2023-10-30
- Tooling für Antennensimulation festlegen	-> Pascal, Tom, Leo, DUE: 2023-11-08
- CAD-System festlegen						-> Lena, Paul, DUE: 2023-11-08