# 2024-02-08 NEMO/DORY coordination meeting

## Attendees
- Pascal Gatti
- Leo
- Martin Stempfle
- Dennis D'Argento
- Edgar Kirchner
- Tom
- Johann

##  DORY Link Budget

- link budget in regard to FOV angle against Nadir
- configurable live script incl. parameters such as antenna type, AIS class, orbit height etc.
- outputs:
	- orbit path against ship position
	- antenna gain against viewing angle
- best link budget might be achieved when not looking at earth in nadir-direction
- transmission characteristics of ship antennas provide the following observations:
	- highest transmission power is present at 52° against Nadir
	- distance to target is dependent on viewing angle -> not an issue for antenna, might impact camera resolution
- question: Does camera and antenna need to point in similar direction?
	- Buffering AIS data may allow for discrepancies in 

- Too many signals may overwhlem signal processing:
- geographical self-managing areas of AIS dictate how many signals can be received at the same time
- field of view angle reduces number of areas visible at same time -> reducing FOV of antenna improves signal processing


Resulting Requirements:
	- Angle to target against nadir range of antenna:
	- maximum antenna gain based on allowable range and orbit height:
	- Maximum number of signals/FOV to be allow for signal processing:
	- Weather effects do not have an effect on the link budget at the given frequency.

## Coordination with DORY - Mechanical

- constraints of antenna types:
	- rod antenna: no longer than 1000m
	- helix antenna: outer diameter no longer than 100mm
	- parabolic antenna: outer diameter no larger than 300mm
- additional aspects:
	- If antenna could be coated in a dielectric material, the antenna gain would improve

## Coordination with NEMO

- requirements are available on ValiSpace and could be used as working
- requirements affecting antenna design:
	- antenna should be able to receive class A
- orbit height: 350km...500km
- optical system is a requirement due to no radar of size being available
- open point: single satellite or constellation
	- three different configurations possible:
		- single satellite with nadir pointing optics and inclined 

## Conclusion

- The following requirements have been defined for the antenna design:
	- The antenna shall be compatible with viewing angle of xx degrees to yy degrees against Nadir to conform to the available transmission power of ship antennas.
	- The antenna shall provide a minimum antenna gain of ZZ dB to allow for receiving signals in the given viewing angle range.
	- The antenna shall provide an antenna with a FOV in a range between a minimum of aa dB and a maximum of bb dB to both present enough usable signals and not overload the signal processing.
	- The antenna may ignore weather effects when calculating the antenna gain due to negligible influence.
	- The antenna shall not violate the following dimensions for the given antenna type:
		- If a monopole/dipole antenna is used, the length of the antenna shall not exceed 1000mm.
		- If a helix antenna is used, the outer diameter shall not exceed 100mm.
		- If a parabolic antenna is used, the outer diameter shall not exceed 300mm.
- The folllowing requirements have been defined for the mission design:
	- The mission shall accomodate an antenna pointing angle within the range of xx degrees to yy degrees against Nadir to conform to the available transmission power distirbution of ship antennas.

