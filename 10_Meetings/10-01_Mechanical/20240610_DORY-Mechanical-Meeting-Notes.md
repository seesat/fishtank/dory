# DORY Mechanical 2024-06-10

## Mission Definition

- mission definition should be finished soon

### Preparation for meeting

- Requirements management
	- check subsystem tree on Github (draw.io diagramme) to consolidate requirements -> Martin -> done
		- most diagrammes do not exist in Valispace
		- transfer done for requirements that seem to still have bearing
		- dicussion of remaining requirements would be beneficial
		
- Meeting Preparation -> Paul
	- time slot: coordination with antenna people before bi-weekly fish tank
	- ask antenna people for current state of Studienarbeit
	- prepare preliminary agenda based on Valispace document
	
### Mechanical Antenna Design

-

### Paper

- link budget
	-> done
- preliminary antenna Design
	-> done mostly
- mechanical concepts
	-> two dipole concepts for now
		-> everyone to think of two antenna concepts and make preliminary sketches
		-> decision next meeting
- simulation results for AIS
	-> partly
- application for other VHF frequencies

### Mechanical Studienarbeit

- Martin will write Studienarbeit at DHBW
- timeframe is free to decide, can be either only in sixth semester or both
- Martin would prefer to use both semesters -> remote work required

- 

## Action Items