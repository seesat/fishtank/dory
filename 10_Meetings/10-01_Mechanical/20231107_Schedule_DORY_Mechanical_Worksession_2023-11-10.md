# Schedule DORY Mechanical Worksession 2023-11-07

## Goals of the Worksession:

- Definition of subsystem tree
- Discussion and definition of initial requirement set
- Identification of interfaces

- Coordination of work packages

## Deliverables:

- Subsystem Tree
- Initial working requirements set
- Interface diagramme
- Work packages for next month

## Schedule:

15:00 - 15:15 - Introduction and Workshop agenda
15:15 - 15:30 - Discussion and addition of missing agenda points

15:30 - 16:00 - Brainstorming for subsystem items
16:00 - 16:30 - Organisation of discussed ideas

16:30 - 16:45 - Break

16:45 - 17:30 - Requirement Definition across tree

17:30 - 18:00 - Workpackage definition for next month

## Organisation

General:
	- Lena moderating
	- Paul taking minutes and notes

15:00 - 15:15 - Introduction and Workshop agenda
	- Short explanation of agenda with goals and deliverables
	- Show agenda on slideshow

15:15 - 15:30 - Discussion and addition of missing agenda points
	- Addition of points to agenda in slideshow
	- Action: Prepare slidshow to fit notes on agenda							-> Lena, 2023-11-09

15:30 - 16:00 - Brainstorming for subsystem items
	- Use draw.io document for collection of ideas
	- Action: Prepare corresponding draw.io document							-> Paul, 2023-11-09
16:00 - 16:30 - Organisation of discussed ideas
	- Continue using draw.io document for rough definition of tree; does not need to be pretty
	- Focus on simplification
	- Action: Add generic structure for subsystem tree to draw.io document		-> Paul, 2023-11-09

16:30 - 16:45 - Break

16:45 - 17:30 - Requirement Definition across tree
	- Use draw.io document for requirement definition
	- Go through subsystem from highest to lowest level
	- Action: Prepare block template that allows for requirement addition		-> Paul, 2023-11-09

17:30 - 18:00 - Workpackage definition for next month
	- Define workpackages based on outcome of worksession
		- Define packages using SMART
		- Examples:
			- Research and add missing information
			- Continue work on existing points, e.g. collect ideas for subsystem
	- Discuss further subsystem development plan
	- Clear up interface definition with functional antenna design
	- Action: Prepare workpackage template										-> Lena, 2023-11-09
		- Description
		- Assignee
		- Due Date
		- Work Order
	
Follow-Up:
	- Action: Organise and clean up worksession results into presentable documents
	- Action: Forward and present results to functional antenna team
	- Action: Continue work on subsystem development acc. to development plan
	- Action: Follow up on agenda points that could not be completed