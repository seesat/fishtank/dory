# DORY Mechanical 2024-06-24

### Paper - Mechanical

- Content:
	- Requirements
	- Design Justification
	- Concept description incl. image
	- Material Trade-off
	- transient behaviour during deployment
	- dynamic behaviour of boom
	
### Mechanical Antenna Design

- thin walled antenna boom
	-> communications check required for design of antenna cross section
	
- forces & energy on tape-springs
	- blossoming is usually mitigated using compression springs
	- will convert to Python code to automate calcs
	- curvature and thickness of tape-spring determines strain energy and therefore torque
	
- Design
	- practical model -> extension to 4 booms planned
	- CAD to be continued
	- tape-spring fixation to be designed
	- height to be reduced

## Action Items

AI-01 Paul: Calculate forces on blooming of boom -> ongoing
AI-02 Paul: Run parameter study on boom geometry -> ongoing
AI-03 Martin: Design a few packing concepts and try to translate one into CAD