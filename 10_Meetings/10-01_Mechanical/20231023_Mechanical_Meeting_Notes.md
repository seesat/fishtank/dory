# 2023-10-23 Mechanical Meeting Notes

## Attendees
- Lena
- Martin
- Paul

## General

- need CAD system
	-> Siemens NX -> ask Bryndis what the issue was
	- Requirements:
		- 3D modelling capabilites
		- generic 3D model export
		- capability for making technical drawings
	- Nice to have:
		- technical analysis capabilities (FEM)
		- PLM, version control
		- standard part data base
	- Options:
		- Siemens NX
		- Siemens SolidEdge
		- Solidworks
		- Dassault 3DX
	- Lena: Ask Edgar about previous research into CAD systems
		
- need document templates
- still need place to store information / research -> Open Action: Paul //2023-10-30

## Worksessions

Could schedule an in-person worksession
Pending slot:
- Fr, Nov 10th 16:00-19:00
	Topics:
	- morphological subsystems
	- preliminary concept definition
	- rough (dis)advantages
	- note open points
	Preparation:
	- Ideas what parts are necessary for an antenna
	- sketch antenna concepts