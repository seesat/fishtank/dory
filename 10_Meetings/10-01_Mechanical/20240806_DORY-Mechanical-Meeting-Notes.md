# DORY Mechanical 2024-08-06

### Paper - Mechanical

- Content:
	- Requirements
	- Design Justification
	- Concept description incl. image
	- Material Trade-off
	- transient behaviour during deployment
	- dynamic behaviour of boom
	
### Mechanical Antenna Design

- thin walled antenna boom
	-> communications check required for design of antenna cross section
	
- forces & energy on tape-springs
	- blossoming is usually mitigated using compression springs
	- will convert to Python code to automate calcs
	- Python code for plate and laminate mechanics done
	- Python code for forces & energy to be written
	
- tape-spring
	- 
	
- Design
	- practical model -> extension to 4 booms planned
	- CAD to be continued
	- tape-spring fixation
		- currently hook-like structure at end of tape-spring
	- components to be checked against real-life examples to check feasibility

## Action Items

AI-01 Paul: Calculate forces on blooming of boom -> ongoing
AI-02 Paul: Run parameter study on boom geometry -> ongoing
AI-03 Martin: Design a few packing concepts and try to translate one into CAD