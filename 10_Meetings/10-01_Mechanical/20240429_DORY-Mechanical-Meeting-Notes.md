# DORY Mechanical 2024-04-29

## Mission Definition

1. Mission Objectives
	-> overall goals
	-> important basic decision:
		- DORY as simple AIS antenna vs. DORY as more general VHF project
	-> objective 1: receiving AIS signals
	-> objective 2: antenna design that can be adjusted to varying frequencies in VHF region
		
2. Mission Needs, Requirements, Constraints
	- needs for objective 1:
		- High performance at AIS frequency
	- needs for objective 2:
		- low required effort to adapt to differing frequencies (high modularity)
		- acceptable performance across design frequencies

3. Mission Concepts
	-> CubeSat antenna -> rather rigid concept
4. Mission Architectures -> Martin
5. System Drivers -> Martin
6. Mission Architecture Characterisation -> Lena

7. Critical Requirements -> Lena
8. Mission Utility -> Paul
9. Mission Concept Selection -> Paul

## Discussion

- additional people for antenna design?
	- maybe lower semesters?
	- TLE?