# 2024-03-25 DORY Mechanical Meeting Notes

## Monopole Telescoping Rod

- Paul showed results
- summary to follow

## Preparation for meeting with antenna people

Presentation:

- one slide for each antenna concept from a mechanical perspective

Questions/Open Points:

- Electrical resistance within antenna -> How much is ok, is there an optimum
- Antenna gain -> What exactly is it, how do we improve it, how much is necessary

Requests:

- Summary of RF design drivers for different antenna types (example: dipole -> diameter, change in diameter, length etc.)
- Performance of thin and long helix antenna (especially quadrifilar type)