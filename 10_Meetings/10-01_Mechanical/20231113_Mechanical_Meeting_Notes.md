# DORY Mechanical 2023-11-13

## Attendees
- Lena
- Martin
- Paul

## General

- need CAD system
	-> Siemens NX -> ask Bryndis what the issue was -> Paul //2023-11-15
	- Requirements:
		- 3D modelling capabilites
		- generic 3D model export
		- capability for making technical drawings
	- Nice to have:
		- technical analysis capabilities (FEM)
		- PLM, version control
		- standard part data base
	- Options:
		- Siemens NX
		- Siemens SolidEdge
		- Solidworks
		- Dassault 3DX
		- Creo
		- CATIA through DHBW

- need document templates -> Paul //2023-11-13
	- started setting up design document

- still need place to store information / research
	- NextCloud for the time being
	- maybe add citation manager with collaboration feature

## Other meetings

n/a

## Worksessions

Could schedule an in-person worksession
Successful completion:
- Fr, Nov 10th 15:00-18:00
	Topics:
	- Subsystem Tree definition
	- initial requirement definition
	- definition of further work packages
	
- Work packages:
	- Summarize ECSS-E-ST-10-06C - Technical requirements specification -> Paul //2023-11-20
	- Summarize ECSS-E-ST.32-10C - Structural factors of safety for spaceflight hardware -> Lena //2023-11-27
	- Define further requirements for the deployment mechanism and its subsystems -> Dominik //2023-12-04
	- Define further requirements for the anetnna structure and its subsystems -> Martin /2023-12-11

## Actions

- Set up document templates -> Paul //2023-11-13
	- first draft done
	- will continue improving until useful
- Ask Prof. Mannchen about getting -> Martin //2023-11-20

## Research

- Lena:
	- Imbirale, W.A.: Space Antenna Handbook. John Wiley & Sons, 2012.
	- Baktur Reyhan: Antenna Design for Cubesats. Artech House, 2021.
	- IEEE Antenna Association
	- Alqaraghuli, Singh and Jornet: Case study on deployable origami antennas for tera-Hertz cubesat networks. In: 2023 International Applied Computational Electromagnetics Society Symposium. IEEE. DOI: 10.23919/ACES57841.2023.10114788
	- Georgakopoulos et al.: Origami antennas. In: IEEE Open Journal of Antennas and Propagation. IEEE. DOI: 10.1109/OJAP.2021.3121102. 

- Paul:
	- AIS Standard: https://www.itu.int/rec/R-REC-M.1371/en
	- RF Handbook