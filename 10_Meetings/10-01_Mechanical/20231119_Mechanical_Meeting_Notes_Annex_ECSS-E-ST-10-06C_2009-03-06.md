# Notes on ECSS-E-ST-10-06C_2009-03-06

## Technical Requirement Specification

### Contents:
The Technical Requirement Specification contains
- the needs of a customer,
- the relavent constraints for a project, and
- the environment of the project.

Generally three major information sets:
1. general info on the document,
- Administrative info
- Scope
- References
- Terms, definitions, abbreviations
2. general info on the context of the document,
- understanding of the project
- information on environment and resulting constraints
- details the different situations of the product/system life cycle
3. technical requirements

### Purpose:
Technical reference for qualification and acceptance of the end product.

### Establishment of technical requirements

Iterative Process of establishing requirements.
0. Early phases define some initial, high-level needs / requirements (Phase 0/A)
1. More precise requirements are defined during development (Phase B)
2. Requirements are frozen once procurement is started (Phase C)

The process of establishing requirements is as follows:

1.1. Identification and evaluation of needs, constraints and environment, expression in terms of technical requirements

1.2. Structuring, classification and justification of individual requirements

1.3. Assessment of full set of technical requirements for correctness, consistency and suitability

1.4. Establishment and release of the preliminary TS

Outcome: intial assessment of project and results in preliminary TS
Purpose: Illustrate need, mission statement, associated evnironmental constraint and programmatic element

2.1. Review of intial TS, identification and proposition of potential concepts

2.2. Structuring, classification and justification of re-newed individual requirements

2.3. Assessment of full re-newed set of technical requirements for correctness, consistency and suitability

2.4. Establishment and release of the re-newed TS

### Types of technical requirements

- functional,
	What the product shall perform.
- mission,
	Task, function, constraint or action resulting from mission scenario.
- interface,
	Interconnection or relationship characteristic.
- environmental,
	Influence of product / system environment during life cycle.
- operational,
	System operability.
- human factors,
	Adaptation to human capabilities.
- (integrated) logistics support,
	Effective and economical support of system for life cycle.
- physical requirements,
	Boundary conditions to ensure physical compatibility that are not defined in interface, design, construction requirements or references.
- product assurance requirements,
	Relevant activities covered by product assurance.
- configuration,
	Composition of product or organization.
- design, and
	Imposed design and construction standards.
- verification requirements
	Imposed verification methods.

## Overall requirements for technical requirements

Excerpt of the standard with most imported points:

Requirements shall be
[...]
1. grouped based on a sorting pattern
2. separately stated
[...]
3. configuration controlled (version controlled)
[...]
4. clauses with supplementary information shall not contain any technical requirements
[...]
5. Technical requirement specifications shall include only technical requirements and as such not contain cost, methods of paymet etc.

## Formulation requirements for technical requirements

1. Performance
- requirements shall be described in quantifiable terms
- if necessary to avoid ambiguity, the method of measurement shall be indicated with the requirements

2. Justification
- requirements should be justified
- it shall be decided which part of the justification to include in the TS

3. Configuration management & traceability
- requirements shall be under configuration management
- requirements shall be backward- (tracing a requirement back to its source) and forward- (tracing the fulfilment of a requirement at the appropriate level) traceable

4. Ambiguity
- requirements shall be unambiguous

5. Uniqueness
- requirements shall be unique

6. Identifiability
- requirements shall be identifiable to relevant function, product and system
- unique identifier shall  be assigned to each technical requirement, that reflects the type of the requirement

7. Singularitiy
- each requirement shall be separately stated

8. Completeness
- requirements shall be self-contained (do not require any additional data or information to express their content)

9. Verification
- requirements shall be verfiable by one or more approved verification methods
- Verification shall be preformed according to ECSS-E-ST-10-02

10. Tolerance
- a tolerance shall be specified for each parameter / variable

General format:

- "what-is-necessary" way of expression, not "how-to"
- expression in positive way as a complete sentence

Verbal forms:

- "shall": requirement
- "should": reccomendation
- "may": permission
- "can": possibility/capability

Format restrictions:

Do not use the following terms (excerpt, full list found in standard):
- "and/or"
- "etc."
- "goal"
- "as far as possible"
- "optimize", "minimize" or "maximize"
- "typical"
- "rapid"
- "sufficient"
- "suitable"