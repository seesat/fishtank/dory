# DORY Mechanical 2024-01-15

## Attendees
- Lena
- Martin
- Paul

## General

- need CAD system - done
	- Requirements:
		- 3D modelling capabilites
		- generic 3D model export
		- capability for making technical drawings
	- Nice to have:
		- technical analysis capabilities (FEM)
		- PLM, version control
		- standard part data base

- CAD coordination:
	- 3Dexperience has group functionality, allowing every member to access CAD data -> use as PLM?
	- will receive lecture documents from Hr. Drescher if everything goes well
	- need system for CAD files
	- Ask Bryndis what current naming scheme on SeeSat-1 is
	- Martin created in group in 3Dexperience
	- software is unintuitive to download

- need document templates
	- started setting up design document
	- prepare specific template for Technical Requirements Specification  -> Paul //2023-12-04 - on hold
		- on hold due to automated Valispace capabilities

- still need place to store information / research
	- NextCloud for the time being
	- maybe add citation manager with collaboration feature

## Valispace

- short introduction to Valispace on 2024-01-15 -> Paul //2024-01-08
- 2024-01-15: introduction done

## Concept Discussion

2024-01-15: preliminary concept discussions:
	- Lena: parabolic antenna
	- Martin: helix antenna
	- Paul: dipole/multipole antenna
	- brainstorming on advantages & disadvantages, points that need further investigation, critical components

## Other meetings

- n/a

## Worksessions

Successful completion:
- Fr, Nov 10th 15:00-18:00
	Topics:
	- Subsystem Tree definition
	- initial requirement definition
	- definition of further work packages
	
- Work packages:
	- Summarize ECSS-E-ST-10-06C - Technical requirements specification -> Paul //2023-11-20
		-> see 20231119_Mechanical_Meeting_Notes_Annex_ECSS-E-ST-10-06C_2009-03-06.md
	- Summarize ECSS-E-ST.32-10C - Structural factors of safety for spaceflight hardware -> Lena //2023-11-27
		-> see Whatsapp group for summary of standard
	- Define further requirements for the antenna structure and its subsystems -> Martin //2023-12-04
		-> see subsystem tree drawing
	- Read through ECSS-E-ST-32 Structural General Requirements, ECSS-E-ST-32 Mechanisms -> Lena, Martin, Paul //2024-01-22

	
2023-11-27:
- Planning workshop in the middle of January for concept discussions based on requirements

2024-01-08:
- weekday evening meetings preferred for January
	- Monday 2024-01-22 - 18:00 as current timeslot
	- 2023-01-15: definition of discussion points

2024-01-15:
- workshop will be about concept discussion for each preliminary antenna type
- discussion of brainstorming results and further steps

## Studienarbeiten

2024-01-15: should have discussions about potential Studienarbeit concepts

## Actions

- Prepare requirements specification template -> Paul //2023-12-04 - on hold
- Ask communications people about Studienarbeit draft -> Paul //2024-01-10
- Send CATIA V5 script to team members -> Lena //2024-01-15 - done
- Ask Philipp Drescher about CATIA documentation -> Lena //2024-01-15 - done
- Test use of Zoom for meetings -> Lena //2024-01-15 - done

## Research

- Lena:
	- Imbirale, W.A.: Space Antenna Handbook. John Wiley & Sons, 2012.
	- Baktur Reyhan: Antenna Design for Cubesats. Artech House, 2021.
	- IEEE Antenna Association
	- Alqaraghuli, Singh and Jornet: Case study on deployable origami antennas for tera-Hertz cubesat networks. In: 2023 International Applied Computational Electromagnetics Society Symposium. IEEE. DOI: 10.23919/ACES57841.2023.10114788
	- Georgakopoulos et al.: Origami antennas. In: IEEE Open Journal of Antennas and Propagation. IEEE. DOI: 10.1109/OJAP.2021.3121102. 

- Paul:
	- AIS Standard: https://www.itu.int/rec/R-REC-M.1371/en
	- RF Handbook
