# DORY Mechanical 2024-05-27

## Mission Definition

### Preparation for meeting

- Requirements management
	- order and filter requirements for mission definition -> Paul
	- add requirements to Valispace based on NEMO-DORY discussions -> Paul
	https://gitlab.com/seesat/fishtank/nemo/-/blob/31dc7d031cea1d5831f87ea5ccecc6974736390d/10_Meetings/20240424_NEMO-DORY_Coordination.md	
	- check subsystem tree on Github (draw.io diagramme) to consolidate requirements -> Martin

- Meeting Preparation -> Lena
	- time slot: coordination with antenna people before bi-weekly fish tank
	- ask antenna people for current state of Studienarbeit
	- prepare preliminary agenda based on Valispace document
