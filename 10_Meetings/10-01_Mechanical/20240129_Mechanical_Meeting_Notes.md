# DORY Mechanical 2024-01-15

## Attendees
- Lena
- Martin
- Paul

## General

- CAD coordination:
	- 3Dexperience has group functionality, allowing every member to access CAD data -> use as PLM?
	- will receive lecture documents from Hr. Drescher if everything goes well
	- need system for CAD files
	- Ask Bryndis what current naming scheme on SeeSat-1 is
	- Martin created in group in 3Dexperience
	- software is unintuitive to download

- need document templates
	- started setting up design document
	- prepare specific template for Technical Requirements Specification  -> Paul //2023-12-04 - on hold
		- on hold due to automated Valispace capabilities

- still need place to store information / research
	- NextCloud for the time being
	- maybe add citation manager with collaboration feature

## Valispace

- short introduction to Valispace on 2024-01-15 -> Paul //2024-01-08
- 2024-01-15: introduction done

## Concept Discussion

2024-01-15: preliminary concept discussions:
	- Lena: parabolic antenna
	- Martin: helix antenna
	- Paul: dipole/multipole antenna
	- brainstorming on advantages & disadvantages, points that need further investigation, critical components

## Other meetings

- n/a

## Worksessions

Successful completion:
- Fr, Nov 10th 15:00-18:00
	Topics:
	- Subsystem Tree definition
	- initial requirement definition
	- definition of further work packages
	
- Work packages:
	- Summarize ECSS-E-ST-10-06C - Technical requirements specification -> Paul //2023-11-20
		-> see 20231119_Mechanical_Meeting_Notes_Annex_ECSS-E-ST-10-06C_2009-03-06.md
	- Summarize ECSS-E-ST.32-10C - Structural factors of safety for spaceflight hardware -> Lena //2023-11-27
		-> see Whatsapp group for summary of standard
	- Define further requirements for the antenna structure and its subsystems -> Martin //2023-12-04
		-> see subsystem tree drawing
	- Read through ECSS-E-ST-32 Structural General Requirements, ECSS-E-ST-32 Mechanisms -> Lena, Martin, Paul //2024-01-22

	
2023-11-27:
- Workshop on 2024-01-15 successfull, scheduled next workshop on 2024-02-12 to continue expanding on concepts

## Studienarbeiten

2024-01-15: should have discussions about potential Studienarbeit concepts
2024-01-29: concepts discussed in workshop, defined workpackages based on that

## Actions

- Prepare requirements specification template -> Paul //2023-12-04 - on hold
- Ask communications people about Studienarbeit draft -> Paul //2024-01-10 - done, posted in Whatsapp group
- Prepare LaTeX concept template -> Paul //2024-01-29 - done
- Compile list of key characteristicsc and figures of merit based on last workshop -> Martin //2024-02-12 - done
- Extend morphological box to include workshop results -> Lena //2024-02-12
- Create basic reference model for CAD based on CubeSat standard -> Paul //2024-02-05
- Schedule meeting with DORY/NEMO to exchange info -> Lena //2024-02-12

## Research

- Lena:
	- Imbirale, W.A.: Space Antenna Handbook. John Wiley & Sons, 2012.
	- Baktur Reyhan: Antenna Design for Cubesats. Artech House, 2021.
	- IEEE Antenna Association
	- Alqaraghuli, Singh and Jornet: Case study on deployable origami antennas for tera-Hertz cubesat networks. In: 2023 International Applied Computational Electromagnetics Society Symposium. IEEE. DOI: 10.23919/ACES57841.2023.10114788
	- Georgakopoulos et al.: Origami antennas. In: IEEE Open Journal of Antennas and Propagation. IEEE. DOI: 10.1109/OJAP.2021.3121102. 

- Paul:
	- AIS Standard: https://www.itu.int/rec/R-REC-M.1371/en
	- RF Handbook
