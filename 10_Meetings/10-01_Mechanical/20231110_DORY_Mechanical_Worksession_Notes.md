# 2023-11-10 Mechanical Worksession Notes

# Subsystem Tree

- Deployment Mechanism
	All parts that play a role in initiating and driving the antenna deployment, as well as the retention after successfull deployment.
	- release actuator
	- deployment actuator
	- retention mechanism


- Antenna structure
	All main load-carrying parts of the antenna (i.e. all parts providing main stiffness).
	Comprised of
	- Payload structure (i.e. all load-carrying parts of the receiver antenna itself)
	- attachment structure (i.e. all load-carrying parts providing a connection to the cubesat itself	

- Housekeeping as optional subsystem, maybe more suitable for avionics

# Philosophy / Overall Goals
 
- DORY shall have a minimised cost
- deployment mechanism shall have a minimum number of parts
- deployment mechanism shall be designed as simple as possible
- teh deployment shall be reversible on ground without any change of parts
 
# Reccommended Reading
 
- ECSS-E-ST-10-06C – Technical requirements specification
- ECSS-E-ST-32-10C Rev.2 Corr.1 – Structural factors of safety for spaceflight hardware

# Workpackages

- Define further requirements for Level 2 subsystems
- Read and summarize important info of above ECSS standards