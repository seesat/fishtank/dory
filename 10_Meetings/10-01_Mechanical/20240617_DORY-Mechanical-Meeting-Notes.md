# DORY Mechanical 2024-06-17
	
### Mechanical Antenna Design

-

### Paper

- link budget
	-> done
- preliminary antenna Design
	-> done mostly
- mechanical concepts
	-> two dipole concepts for now
		-> everyone to think of two antenna concepts and make preliminary sketches
		-> decision next meeting
- simulation results for AIS
	-> partly
- application for other VHF frequencies

### Mechanical Studienarbeit

- Martin will write Studienarbeit at DHBW
- timeframe is free to decide, can be either only in sixth semester or both
- Martin would prefer to use both semesters -> remote work required

- 

## Action Items