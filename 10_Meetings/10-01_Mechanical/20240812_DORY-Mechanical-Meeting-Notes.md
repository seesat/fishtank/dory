# DORY Mechanical 2024-08-12

### Paper - Mechanical

- Content:
	- Requirements
	- Design Justification
	- Concept description incl. image
	- Material Trade-off
	- transient behaviour during deployment
	- dynamic behaviour of boom
	
### Mechanical Antenna Design

- thin walled antenna boom
	-> communications check required for design of antenna cross section
	
- forces & energy on tape-springs
	- blossoming is usually mitigated using compression springs
	- will convert to Python code to automate calcs
	- Python code for plate and laminate mechanics done
	- Python code for forces & energy in work
	
- boom dynamics
	- Lena will continue simulations to get baseline on material influence on fundamental modes
	- Meeting on Wednesday, 14.08.24, 19:00
	
- tape-spring
	- mech. working values:
		- sector angle: 135°
		- thickness: 0.25mm
		- curvature radius = 8mm to 12mm
		- reel inner diameter: 50mm...60mm
			- defined as working value since requirement for antenna length is not fully defined 
		- reel outer fence diameter: 80mm
			- defined as working value that estimates space of 10mm to boundary of 
		-> implement values into CAD -> Martin

	-> ask Tom to simulate three antennas with the given values (8,10,12mm curvature radius) -> 
	- check influence on antenna (length, antenna gain etc.)
	
- VHF applications
	- Question: Which use cases in VHF frequency could be argued for -> Lena
	- 
	
- Design
	- practical model -> extension to 4 booms planned
	- CAD to be continued
	- tape-spring fixation
		- currently hook-like structure at end of tape-spring
	- components to be checked against real-life examples to check feasibility

## Action Items

AI-01 Paul: Calculate forces on blooming of boom -> ongoing
AI-02 Paul: Run parameter study on boom geometry -> ongoing
AI-03 Martin: Design a few packing concepts and try to translate one into CAD# DORY Mechanical 2024-08-12

### Paper - Mechanical

- Content:
	- Requirements
	- Design Justification
	- Concept description incl. image
	- Material Trade-off
	- transient behaviour during deployment
	- dynamic behaviour of boom
	
### Mechanical Antenna Design

- thin walled antenna boom
	-> communications check required for design of antenna cross section
	
- forces & energy on tape-springs
	- blossoming is usually mitigated using compression springs
	- will convert to Python code to automate calcs
	- Python code for plate and laminate mechanics done
	- Python code for forces & energy in work
	
- boom dynamics
	- Lena will continue simulations to get baseline on material influence on fundamental modes
	- Meeting on Wednesday, 14.08.24, 19:00
	
- tape-spring
	- mech. working values:
		- sector angle: 135°
		- thickness: 0.25mm
		- curvature radius = 8mm to 12mm
		- reel inner diameter: 50mm...60mm
			- defined as working value since 
		- reel outer fence diameter: 80mm
			- defined as working value that estimates space of 10mm to boundary of 
		-> implement into 

	-> ask Tom to simulate three antennas with the given values (8,10,12mm curvature radius) -> 
	- check influence on antenna (length, antenna gain etc.)
	
- VHF applications
	- Question: Which use cases in VHF frequency
	- Re
	
- Design
	- practical model -> extension to 4 booms planned
	- CAD to be continued
	- tape-spring fixation
		- currently hook-like structure at end of tape-spring
	- components to be checked against real-life examples to check feasibility

## Action Items

AI-01 Paul: Calculate forces on blooming of boom -> ongoing
AI-02 Paul: Run parameter study on boom geometry -> ongoing
AI-03 Martin: Design a few packing concepts and try to translate one into CAD