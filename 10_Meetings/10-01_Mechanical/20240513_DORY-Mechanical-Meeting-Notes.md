# DORY Mechanical 2024-05-13

## NEMO-DORY coordination

- mech. Begrenzungen und Anforderungen
	- maximum dimensions
		- height, width: max. 100mm
		- length: 1200mm
	- Machbarkeit von Antennenformen
		- Parabolic
			-> hard to stow, complex shape due to parabolic arc
			-> KaPDA as reference mission (but aperture would need to be much larger than KaPDA, infeasible)
			-> low preference due to infeasibility
		- Helix
			-> mechanical complexity low
			-> easily stowable
			-> feasible if material with sufficient torsional strength available
		- Monopole/Dipole
			-> mechanical complexity low to medium depending on design
			-> easily stowable
			-> high level of maturity
			-> feasible, complexity depending on design
	- Materialauswahl
		- Aluminium (EN-AW 7075T6, EN-AW 6061)
		- Stainless Steel (1.4401, 1.4301)
		- Copper alloy (use pure copper for now)
		- CFRP (assume antenna axis parallel to fibers)
	- Satellitenstruktur
	- aktuell Größe undefiniert, Arbeitswert 8...12U
	- Antennensimulation weiterhin mit 2U, um Vergleichbarkeit zu gewährleisten
	- undefiniert, da Plattform aktuell unbestimmt


## Mission Definition

1. Mission Objectives
	-> overall goals
	-> important basic decision:
		- DORY as simple AIS antenna vs. DORY as more general VHF project
	-> objective 1: receiving AIS signals
	-> objective 2: antenna design that can be adjusted to varying frequencies in VHF region
	
	-> consult current state of NEMO mission design
	
2. Mission Needs, Requirements, Constraints
	- needs for objective 1:
		- High performance at AIS frequency
	- needs for objective 2:
		- low required effort to adapt to differing frequencies (high modularity)
		- acceptable performance across design frequencies

3. Mission Concepts
	-> CubeSat antenna -> rather rigid concept
4. Mission Architectures -> Martin
	- try to get broad overview of influencing factors
	- check which aspects could be changed (traded)
	- define which options are available in general
	- select a few viable options based on mission
	- system drivers as first parameters to be varied in trade overview
	- definition of trade-off tree based on possible parameters
5. System Drivers -> Martin
	- parameters that have largest impact on cost, performance, risk or schedule *and* are subject to designer's choice
	- most common (filtered for DORY Mechanical):
		- size
		- mass
		- power
6. Mission Architecture Characterisation -> Lena
	- last step in mission characterization
		-> deepest mission analysis
	- most important steps:
		- Definition preliminary concept
		- definition of components and their characterization
		- orbits and constellations
		- payload size and performance
		- mission operation approach
		- mission environment design

7. Critical Requirements -> Lena
	- identification of critical Requirements
	- requirements that dominate design and have very large impact on performance and cost
8. Mission Utility -> Paul
	- impact of performance characteristics on mission objects
	- computing impact of performance parameters on use case
9. Mission Concept Selection -> Paul
	- find optimum of risk, schedule, cost
	- decision is often not fully technically-driven

