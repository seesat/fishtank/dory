# DORY Mechanical 2023-10-30

## Attendees
- Lena
- Martin
- Paul

## General

- need CAD system
	-> Siemens NX -> ask Bryndis what the issue was
	- Requirements:
		- 3D modelling capabilites
		- generic 3D model export
		- capability for making technical drawings
	- Nice to have:
		- technical analysis capabilities (FEM)
		- PLM, version control
		- standard part data base
	- Options:
		- Siemens NX
		- Siemens SolidEdge
		- Solidworks
		- Dassault 3DX
	- Lena: Ask Edgar about previous research into CAD systems -> no results yet, referred to other students
		
- need document templates -> Paul //2023-11-13
- still need place to store information / research -> Open Action: Paul //2023-10-30
	- NextCloud for the time being
	- maybe add citation manager with collaboration feature
	
## Other meetings

2023-11-07, 19:00-20:00 - Worksession Preparation

## Worksessions

Could schedule an in-person worksession
Pending slot:
- Fr, Nov 10th 16:00-19:00
	Topics:
	- morphological subsystems
	- preliminary concept definition
	- rough (dis)advantages
	- note open points
	Preparation:
	- Ideas what parts are necessary for an antenna
	- sketch antenna concepts

## Actions

- Set up document templates -> Paul //2023-11-13

## Research

- Lena:
	- Imbirale, W.A.: Space Antenna Handbook. John Wiley & Sons, 2012.
	- Baktur Reyhan: Antenna Design for Cubesats. Artech House, 2021.
	- IEEE Antenna Association
	- Alqaraghuli, Singh and Jornet: Case study on deployable origami antennas for tera-Hertz cubesat networks. In: 2023 International Applied Computational Electromagnetics Society Symposium. IEEE. DOI: 10.23919/ACES57841.2023.10114788
	- Georgakopoulos et al.: Origami antennas. In: IEEE Open Journal of Antennas and Propagation. IEEE. DOI: 10.1109/OJAP.2021.3121102. 
	
- Paul:
	- AIS Standard: https://www.itu.int/rec/R-REC-M.1371/en
	- RF Handbook