# DORY Mechanical 2024-06-24

### Paper - Mechanical

- Content:
	- Requirements
	- Design Justification
	- Concept description incl. image
	- Material Trade-off
	- transient behaviour during deployment
	- dynamic behaviour of boom
	
### Mechanical Antenna Design

- thin walled antenna boom
	-> communications check required for design of antenna cross section

### Mechanical Studienarbeit

- Martin will write Studienarbeit at DHBW
- timeframe is free to decide, can be either only in sixth semester or both
- Martin would prefer to use both semesters -> remote work required

## Action Items

AI-01 Paul: Calculate forces on blooming of boom
AI-02 Paul: Run parameter study on boom geometry
AI-03 Martin: Design a few packing concepts and try to translate one into CAD