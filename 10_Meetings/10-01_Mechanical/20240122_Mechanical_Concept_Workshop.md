# 2024-01-22 DORY Mechanical Workshop

## Parabolic antenna
- strong directional characteristic but good signal reception within cone
- Existing concepts:
	- KaPDA -> Parabolic mesh antenna, parabolic dish folded along half-radius, 1.5U size, frequency of 30 GHz
- Discussion:
	- good mechanism concept
	- frequency as largest problem due to increased size
	- many moving parts -> high complexity, more points of failure
	- meshes sensitive to outside forces -> higher risk of damage
	- potential shadowing of downlink antenna
	- large aperture for assembly volume
	- relatively low structural weight
	- open point: number of single point failures
	- open point: required directional accuracy

## Helix antenna
- spiral(helix) shaped conductor around zylindrical structure
- frequency is defined by number of coils and diameter
- signals with circular polarisation may be received without continuous attitude adjustment
- broad working band
- signal interference by near components can be an issue
- first calc by antenna team estimates length of ~2.3m and diameter of 0.6m
- deployment could be done by coil tension of conductor
- Discussion:
	- can be stored in very small volume due to spring-like compression
	- conductor material needs to be resistant to work-hardening to avoid breaking
	- issue: damping of deployment oscillations and energy dissipation
	- large area upon which drag forces can act, large lever for these forces -> constant disturbance
	- integrated conductor and structure -> weight saving
	- hard to test on-ground, but test concepts with gravity off-loading could be used

## Dipole antenna
- multiple concepts:
- Discussion:
	- folded segmented beam
		- each spring is a single point failure
		- influence of segmentation on antenna signal quality
		- very modular (same part multiple parts
		- deployment damping as an issue
		- low number of active components
		- parallelisation can be used to reduce number of of segments in sequence
		- openpoint: damping of signal by antenna structure

	- pressure tube
		- leak tightness against vacuum
		- storage of conductor?
		- pressure vessel -> safety?

	- rolled thin beam
		- thin beam that is rolled up in a direction 
		- option: storing beam rolled around longitudinal axis letting it deploy under inner tension in transversal direction
		- no segmentation allows for keeping antenna in one piece
		- very compact storage (compare measuring tape)
		- open point: which materials allow for stress cycles -> ductile materials
		- integrated conductor and structure might be possible
		- problem: bending stiffness
		
	- telescoping rod
		- very compact
		- low weight due to circular cross-section
		- high bending stiffness
		- possible integration of conductor and structure
		- open point: influence of hollow structure on signal strength
		- open point: influence of distribution of cross-section on flexural stiffness and eigen-modes

## Workpackages

- Lena: extend morphological box to include todays concepts

- Martin: compile list of key characteristics and figures of merit of concepts based on today's discussions

- Paul: define LaTex concept template

## Scheduling

- next workshop on 2024-02-12, 18:00-20:00
