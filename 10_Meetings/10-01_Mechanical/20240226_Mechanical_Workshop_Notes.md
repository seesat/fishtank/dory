# DORY Mechanical Workshop on 2024-02-26

## Attendees
- Lena
- Martin
- Paul

## General
- presentation of the workpackage about the Parabolic Antenna (Lena)
	-> in general this concept is not worth pursuing
	-> a document with the general information will be written anyway to summarize the problems and justify the decision 

- presentation of the workpackage about the Helix Antenna (Martin)
	-> concept seems very promising
	-> the first part of the evaluation of this concept was made and can be found on GitLab
	-> a document with the general information, possible configurations and problems will be written and uploaded

- the presentation of the workpackage about the Dipole/Multipole Antenna (Paul) was postponed due to lack of time during the workshop and will be held during the next mechanical meeting on 2024-03-11

- some of the documents used in the Studienarbeit can not be found in the online library of the DHBW Ravensburg
	-> the authors will be asked during the next meeting
	-> otherwise Lena has contacts to the DHBW Stuttgart and Mannheim

## Other meetings

- next Worksession on 2024-03-11 18:00-19:30
	Agenda:
	- Presentation of the workpackage about the Dipole/Multipole Antennas (Paul)
	- discussion of possible topics and questions for a meeting with the entire DORY-Team
	- preparation of the results to be presented during said meeting

- Meeting with the entire DORY-Team on 2024-04-10
	- presentation of the respective results
	- discussion of the plan for the next months


## Actions

- Prepare requirements specification template -> Paul //2023-12-04 - on hold
- Ask communications people about Studienarbeit draft -> Paul //2024-01-10 - done, posted in Whatsapp group
- Prepare LaTeX concept template -> Paul //2024-01-29 - done
- Compile list of key characteristicsc and figures of merit based on last workshop -> Martin //2024-02-12 - done
- Extend morphological box to include workshop results -> Lena //2024-02-12 - done
- Create basic reference model for CAD based on CubeSat standard -> Paul //2024-02-05
- Schedule meeting with DORY/NEMO to exchange info -> Lena //2024-02-12 - done
- Prepare Workshop -> Lena // 2024-02-26 - done
- Prepare and present Workpackages -> all // 2024-02-26 - partially done
		- prensent Workpackage -> Paul // 2024-03-11
- Preparation of a summarizing Document for each Workpackage -> Martin, Lena // 2024-03-11

## Research

- Lena:
	- Imbirale, W.A.: Space Antenna Handbook. John Wiley & Sons, 2012.
	- Baktur Reyhan: Antenna Design for Cubesats. Artech House, 2021.
	- IEEE Antenna Association
	- Alqaraghuli, Singh and Jornet: Case study on deployable origami antennas for tera-Hertz cubesat networks. In: 2023 International Applied Computational Electromagnetics Society Symposium. IEEE. DOI: 10.23919/ACES57841.2023.10114788
	- Georgakopoulos et al.: Origami antennas. In: IEEE Open Journal of Antennas and Propagation. IEEE. DOI: 10.1109/OJAP.2021.3121102. 

- Paul:
	- AIS Standard: https://www.itu.int/rec/R-REC-M.1371/en
	- RF Handbook

