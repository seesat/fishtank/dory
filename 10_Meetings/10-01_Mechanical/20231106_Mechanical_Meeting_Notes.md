# DORY Mechanical 2023-11-06

## Attendees
- Dominik
- Martin
- Paul

## General

- need CAD system
	-> Siemens NX -> ask Bryndis what the issue was -> Paul //2023-11-10
	- Requirements:
		- 3D modelling capabilites
		- generic 3D model export
		- capability for making technical drawings
	- Nice to have:
		- technical analysis capabilities (FEM)
		- PLM, version control
		- standard part data base
	- Options:
		- Siemens NX
		- Siemens SolidEdge
		- Solidworks
		- Dassault 3DX
		- Creo

- need document templates -> Paul //2023-11-13
	- started setting up design document

- still need place to store information / research -> Paul //2023-10-30
	- NextCloud for the time being
	- maybe add citation manager with collaboration feature

## Other meetings

2023-11-07, 19:00-20:00 - Worksession Preparation

## Worksessions

Could schedule an in-person worksession
Pending slot:
- Fr, Nov 10th 15:00-18:00
	Topics:
	- requirements
	
	( - morphological subsystems
	- preliminary concept definition
	- rough (dis)advantages
	- note open points)
	Preparation:
	- Ideas what parts are necessary for an antenna
	- sketch antenna concepts

## Actions

- Set up document templates -> Paul //2023-11-13
	- first draft done
	- will continue improving until useful

## Research

- Lena:
	- Imbirale, W.A.: Space Antenna Handbook. John Wiley & Sons, 2012.
	- Baktur Reyhan: Antenna Design for Cubesats. Artech House, 2021.
	- IEEE Antenna Association
	- Alqaraghuli, Singh and Jornet: Case study on deployable origami antennas for tera-Hertz cubesat networks. In: 2023 International Applied Computational Electromagnetics Society Symposium. IEEE. DOI: 10.23919/ACES57841.2023.10114788
	- Georgakopoulos et al.: Origami antennas. In: IEEE Open Journal of Antennas and Propagation. IEEE. DOI: 10.1109/OJAP.2021.3121102. 

- Paul:
	- AIS Standard: https://www.itu.int/rec/R-REC-M.1371/en
	- RF Handbook