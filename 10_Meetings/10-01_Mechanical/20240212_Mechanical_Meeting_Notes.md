# DORY Mechanical Worksession 2024-02-12

## Attendees
- Lena
- Martin
- Paul

## General
- review of meeting with Nemo/Dory on 2024-02-08 -> Meeting minutes can be found on GitLab
- review of the revised morphological box -> rsults can be found on GitLab
- definition of workpackages
- short discussion about the organisational structure of the project
	-> mission definition review might be useful for a general overview
	-> am monthly meeting could be a good possibility to provide more exchange throughout the teams

## Work Packages
- the objective of these workackages is to define the possible antenna concepts further and create a base for their evaluation
- this encompasses rough calculations and sketches
- the evaluation scheme can be used as a mode of orientation
-> partcularly critical points encountered during design shall be emphazied and documented!
- all concepts shall have a maximum volume of 0.5 U and a maximum mass of 0.8 kg

notes for each antenna type:

- Parabol Antenna
	- a rough projection of the diameter should be carried out beforehand -> the screen tends to be significantly larger (possibly around 35 cm diameter)
	- Concept can be based on the KaPDA, but can also be new
	-> go a little deeper into the folding mechanism due to the complexity

- Helix Antenna
	- Take rigidity into account (consider material if necessary)
		-> find a good compromise between stability, ductility and conductivity 
	- Take another closer look at 3CAT-4 (AIS antenna may not be the helix antenna)

- Dipole/Multipole Antenne
	- Segmentation primarily of high relevance
		-> easiest and simplest method
		-> Effect of segmentation on the antenna and its performance
	- Rough estimate of stiffness + choice of material


## Other meetings

- next Worksession on 2024-02-26 19:00-21:00
	Agenda:
	- Presentation and discussion of workpackages
	- reflection of the problems and solutions encountered during work
	- definition of new work packages based on the results


## Actions

- Prepare requirements specification template -> Paul //2023-12-04 - on hold
- Ask communications people about Studienarbeit draft -> Paul //2024-01-10 - done, posted in Whatsapp group
- Prepare LaTeX concept template -> Paul //2024-01-29 - done
- Compile list of key characteristicsc and figures of merit based on last workshop -> Martin //2024-02-12 - done
- Extend morphological box to include workshop results -> Lena //2024-02-12 - done
- Create basic reference model for CAD based on CubeSat standard -> Paul //2024-02-05
- Schedule meeting with DORY/NEMO to exchange info -> Lena //2024-02-12 - done
- Prepare Workshop -> Lena // 2024-02-26
- Prepare and present Workpackages -> all // 2024-02-26


## Research

- Lena:
	- Imbirale, W.A.: Space Antenna Handbook. John Wiley & Sons, 2012.
	- Baktur Reyhan: Antenna Design for Cubesats. Artech House, 2021.
	- IEEE Antenna Association
	- Alqaraghuli, Singh and Jornet: Case study on deployable origami antennas for tera-Hertz cubesat networks. In: 2023 International Applied Computational Electromagnetics Society Symposium. IEEE. DOI: 10.23919/ACES57841.2023.10114788
	- Georgakopoulos et al.: Origami antennas. In: IEEE Open Journal of Antennas and Propagation. IEEE. DOI: 10.1109/OJAP.2021.3121102. 

- Paul:
	- AIS Standard: https://www.itu.int/rec/R-REC-M.1371/en
	- RF Handbook

